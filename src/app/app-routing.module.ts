import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CRMModule } from '../../projects/crm/src/app/app.module';
import { NewsModule } from '../../projects/news/src/app/app.module';
import { AccessModule } from '../../projects/access/src/app/app.module';

import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent, data: { title: '16 AHEAD - Home'}, pathMatch: 'full' },
  // { path: 'access', loadChildren: '../../projects/access/src/app/app.module#AccessModule' },
  { path: 'contacts', loadChildren: '../../projects/crm/src/app/app.module#CRMModule' },
  { path: 'news', loadChildren: '../../projects/news/src/app/app.module#NewsModule' },
  // { path: '**', redirectTo: '/access' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes),  AccessModule.forRoot(), CRMModule.forRoot(), NewsModule.forRoot()],
  exports: [RouterModule]
})
export class AppRoutingModule { }
