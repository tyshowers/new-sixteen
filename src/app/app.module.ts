import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { CRMModule } from '../../projects/crm/src/app/app.module';
import { NewsModule } from '../../projects/news/src/app/app.module';
import { AccessModule } from '../../projects/access/src/app/app.module';

import { MasterNavComponent } from './master-nav/master-nav.component';
import { HomeComponent } from './home/home.component';
import { SixteenCustomerHeader1Module } from 'sixteen-customer-header1';
import { SixteenCustomerAboutProduct1Module } from 'sixteen-customer-about-product1';

@NgModule({
  declarations: [
    AppComponent,
    MasterNavComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    SixteenCustomerHeader1Module,
    SixteenCustomerAboutProduct1Module,
    AppRoutingModule,
    AccessModule.forRoot(),
    CRMModule.forRoot(),
    NewsModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
