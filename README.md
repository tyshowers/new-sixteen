# 16AHEAD

[Production Site](https://www.16ahead.com)

[Development Site](https://dev.16ahead.com)

##Vision
* Eliminate App Fatigue
* Familiar look and feel to process
* Pro-active data analysis and suggestion
* Find instead of searching
* Can’t get a clear picture of your business if email is separate from your e-commerce and your sales opportunities are separate from your calendar
* Most systems you put information in and when you need the information, you go retrieve it, not with 16AHEAD, once you put information in, the AI goes to work analyzing information and sends you alerts on what you should be doing

##Summary
16AHEAD is used to make  business run smoothly and efficiently. Serious consideration must be given to how information is processed and used throughout an organization. The software programs used should enhance the business not weigh it down and make it react sluggishly or not at all.

Often, businesses are challenged by: lack of vision, infrastructure and a transparent management system. Using 16AHEAD helps create a vision, build the necessary infrastructure and operate a management system that keeps a pulse of business activity.

#Business Goals

Automate as many processes as possible. An effective organization does not waste time on non-essential tasks but instead places that energy on building the brand.

Through research and development, we've found the following functionalities help build a successful business:

- Lead Generation and Conversion
- Build a strong sales team with lead generation, online forms, opportunity and relationship tracking.
- Client Fulfillment
- The request/fulfillment system gives you a better handle on how quickly client requests are fulfilled. By operating in an open environment the entire staff can address overlooked items with the visual alert system.
- Empower Management
Use charts to review real-time snapshots of each employee's activities and progress.
- Implement Strategy and Monitor Progress
Using activity-centric features brings true accountability throughout the company.
- Analyze Prospect and Client Demographics
Customize reports on the fly for realtime information from any department.
- Keep the Company Mission Top-of-Mind
Display your company's mission statement on each user's home page.

The use of a data focused application with an integrated process will help a business become more predictable and lead to improving not only staff productivity but client services as well.

16AHEAD is an attempt to be complete enterprise solution that can be set up in a few minutes at a fraction of the price of comparative systems. If you are affected by any of the following, 16AHEAD can save you time and dollars. A must have application if you suffer from any of the following:

- Disconnected Communication
- No Data Alignment
- Personnel in Multiple Locations
- Poor Document Sharing
- No Project Communication Among Clients and Suppliers
- Poor Customer Service
- No Calendar Sharing
- Software deployment issues
- Lost Leads
- Multiple Applications
- No Real-time Data Analysis
- Disconnected Shopping Cart, Inventory Management, Contact Management and Lead Tracking


##Features and Benefits
* No double data entry
* Collaborate on ANYTHING (just uncheck draft)
* True workflow with Next button guide
* Find instead of search
* Data is monitored and suggested actions/alerts are pushed
* Document/Contact management in one place
* Realtime company snapshot
* Realtime project status on spending, estimate, remaining, and spent
* Contextually aware data
* Fast
* Share or not share by using private mode
* Automatic data sharing/visibility
* Contact dashboard in addition to sales, blog, opportunity, property and project
* Expands as you grow, pay for what you use
* Seamless integration
* Goals
* Topics for Knowledge Management
* Seamlessly transition contact management to email to project management
* Improved “data alignment”
* Can Improve customer retention with all relevant information in once place
* AI tries to understand customer behavior
* AI recommends which people should be contacted based on a variety of inputs that can only be achieved with an integrated system that houses all relevant data
* Increase communication between departments
* Increase awareness of department activities
* Messaging built in
* Increase manager knowledge
* Practical pre-sales tracking with opportunities
* No need for reports with the comprehensive dashboard
* Complex information is simplified
* Proactive information pushed to you
* Adaptability
* Message, task, alert notifications (email coming soon)
* Turn on and off modules
* Create and track paid events
* Responsive . . . works on any screen
* Help on every page
* Create multiple blogs and online stores
* Group products into bundles
* Create product offers with expiration date
* Typeahead learning
* Integrated News  
* Pick from a variety of news sources
* News and data sharing to messages
* Favor and bookmark anything
* AI ranks contacts based on various factors
* Menu changes based on data context
* Task, message and alert indicators
* Show/hide side menus
* Automatic offline mode (needs more testing)
* Post info or news to group chat
* Pop up help dialog boxes
* deleted items show as light grey text
* Administrators see everything
* 3 user roles, Read only, Authors/Editors and Administrators
* Property Module
* Organize pre-canned actions as a toolbar to expedite your work


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.7.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
