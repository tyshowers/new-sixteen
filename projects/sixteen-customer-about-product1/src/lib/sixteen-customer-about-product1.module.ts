import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerAboutProduct1Component } from './sixteen-customer-about-product1.component';

@NgModule({
  declarations: [SixteenCustomerAboutProduct1Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerAboutProduct1Component]
})
export class SixteenCustomerAboutProduct1Module { }
