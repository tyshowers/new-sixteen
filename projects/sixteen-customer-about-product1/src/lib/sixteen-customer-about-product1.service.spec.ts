import { TestBed } from '@angular/core/testing';

import { SixteenCustomerAboutProduct1Service } from './sixteen-customer-about-product1.service';

describe('SixteenCustomerAboutProduct1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerAboutProduct1Service = TestBed.get(SixteenCustomerAboutProduct1Service);
    expect(service).toBeTruthy();
  });
});
