import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerAboutProduct1Component } from './sixteen-customer-about-product1.component';

describe('SixteenCustomerAboutProduct1Component', () => {
  let component: SixteenCustomerAboutProduct1Component;
  let fixture: ComponentFixture<SixteenCustomerAboutProduct1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerAboutProduct1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerAboutProduct1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
