import { TestBed } from '@angular/core/testing';

import { SixteenCarouselService } from './sixteen-carousel.service';

describe('SixteenCarouselService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCarouselService = TestBed.get(SixteenCarouselService);
    expect(service).toBeTruthy();
  });
});
