import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCarouselComponent } from './sixteen-carousel.component';

describe('SixteenCarouselComponent', () => {
  let component: SixteenCarouselComponent;
  let fixture: ComponentFixture<SixteenCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
