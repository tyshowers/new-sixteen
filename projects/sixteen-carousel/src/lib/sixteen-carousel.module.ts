import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCarouselComponent } from './sixteen-carousel.component';
import { SixteenDataModule } from 'sixteen-data';
import { OwlModule } from 'ngx-owl-carousel';

@NgModule({
  declarations: [SixteenCarouselComponent],
  imports: [
    CommonModule,
    RouterModule,
    SixteenDataModule,
    OwlModule
  ],
  exports: [SixteenCarouselComponent]
})
export class SixteenCarouselModule { }
