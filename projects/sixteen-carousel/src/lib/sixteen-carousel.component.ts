import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Carousel } from 'sixteen-data';
import { OwlCarousel } from 'ngx-owl-carousel';

@Component({
  selector: 'lib-sixteen-carousel',
  templateUrl: './sixteen-carousel.component.html',
  styles: []
})
export class SixteenCarouselComponent implements OnInit {

  @ViewChild('owlElement') owlElement: OwlCarousel;
  @Input() carousel: Carousel[];
  @Input() animateOut = 'slideOutDown';
  @Input() animateIn = 'fadeInRight';
  @Input() dots = true;
  @Input() loop = true;
  @Input() playSpeed = 1500;
  @Input() hoverPause = true;
  @Input() navigation = true;
  @Input() nav = false;
  @Input() autoPlay = true;
  @Input() numberOfItemsToDisplay = 1;

  carouselOptions: any;
  @Input() defaultFont = 'Verdana';
  @Input() defaultColor = '#fff';
  @Input() defaultMaxWidth = '300px';
  @Input() defaultFontWeight = 'lighter';
  @Input() defaultPadding = '10px';
  @Input() defaultBorderRadius = '5px';

  @Input() rgba1_red = 58;
  @Input() rgba1_green = 152;
  @Input() rgba1_blue = 220;
  @Input() rgba2_red = 17;
  @Input() rgba2_green = 45;
  @Input() rgba2_blue = 58;
  @Input() isRadialBackground = false;

  images = [];


  constructor() { }

  ngOnInit() {
    this.setCarouselOptions();
    this.setCarouselItems();
    console.log(this.carousel);
  }

  fun() {
    this.owlElement.next([200])
  }

  private setCarouselItems() : void {
    this.carousel.forEach((c) => {
      console.log(c.url);
      this.images.push(c.url)
    })
  }

  private setCarouselOptions() : void {
    this.carouselOptions = {
      animateOut: this.animateOut,
      animateIn: this.animateIn,
      dots: this.dots,
      loop: this.loop,
      autoplaySpeed: this.playSpeed,
      autoplayHoverPause: this.hoverPause,
      navigation: this.navigation,
      nav: this.nav,
      autoplay: this.autoPlay,
      items: this.numberOfItemsToDisplay
    };
  }

}
