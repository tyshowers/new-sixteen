/*
 * Public API Surface of sixteen-carousel
 */

export * from './lib/sixteen-carousel.service';
export * from './lib/sixteen-carousel.component';
export * from './lib/sixteen-carousel.module';
