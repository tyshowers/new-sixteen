/*
 * Public API Surface of sixteen-customer-header2
 */

export * from './lib/sixteen-customer-header2.service';
export * from './lib/sixteen-customer-header2.component';
export * from './lib/sixteen-customer-header2.module';
