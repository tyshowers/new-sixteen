import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerHeader2Component } from './sixteen-customer-header2.component';

@NgModule({
  declarations: [SixteenCustomerHeader2Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerHeader2Component]
})
export class SixteenCustomerHeader2Module { }
