import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lib-sixteen-customer-header2',
  templateUrl: './sixteen-customer-header2.component.html',
  styles: []
})
export class SixteenCustomerHeader2Component implements OnInit {

  @Input() headingText = 'Present your awesome product';
  @Input() descriptionText = 'Delectus, ut perspiciatis, unde omnis voluptas nulla vero. Facilis est laborum et harum quidem rerum necessitatibus saepe. Magni dolores et molestiae consequatur, vel illum, qui.';
  @Input() button1Text = 'Sign Up';
  @Input() button1Link;
  @Input() button1Visible = true;
  @Input() button2Text = 'Read More';
  @Input() button2Link;
  @Input() button2Visible = true;
  @Input() imageURL = "http://via.placeholder.com/600x1220";


  constructor() { }

  ngOnInit() {
  }

}
