import { TestBed } from '@angular/core/testing';

import { SixteenCustomerHeader2Service } from './sixteen-customer-header2.service';

describe('SixteenCustomerHeader2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerHeader2Service = TestBed.get(SixteenCustomerHeader2Service);
    expect(service).toBeTruthy();
  });
});
