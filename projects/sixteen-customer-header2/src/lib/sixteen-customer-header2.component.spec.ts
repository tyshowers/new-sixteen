import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerHeader2Component } from './sixteen-customer-header2.component';

describe('SixteenCustomerHeader2Component', () => {
  let component: SixteenCustomerHeader2Component;
  let fixture: ComponentFixture<SixteenCustomerHeader2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerHeader2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerHeader2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
