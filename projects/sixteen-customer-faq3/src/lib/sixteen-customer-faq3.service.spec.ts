import { TestBed } from '@angular/core/testing';

import { SixteenCustomerFaq3Service } from './sixteen-customer-faq3.service';

describe('SixteenCustomerFaq3Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerFaq3Service = TestBed.get(SixteenCustomerFaq3Service);
    expect(service).toBeTruthy();
  });
});
