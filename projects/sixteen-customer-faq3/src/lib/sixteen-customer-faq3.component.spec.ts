import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerFaq3Component } from './sixteen-customer-faq3.component';

describe('SixteenCustomerFaq3Component', () => {
  let component: SixteenCustomerFaq3Component;
  let fixture: ComponentFixture<SixteenCustomerFaq3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerFaq3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerFaq3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
