import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerFaq3Component } from './sixteen-customer-faq3.component';

@NgModule({
  declarations: [SixteenCustomerFaq3Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerFaq3Component]
})
export class SixteenCustomerFaq3Module { }
