/*
 * Public API Surface of sixteen-customer-faq3
 */

export * from './lib/sixteen-customer-faq3.service';
export * from './lib/sixteen-customer-faq3.component';
export * from './lib/sixteen-customer-faq3.module';
