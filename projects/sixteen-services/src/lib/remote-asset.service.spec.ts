import { TestBed } from '@angular/core/testing';

import { RemoteAssetService } from './remote-asset.service';

describe('RemoteAssetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RemoteAssetService = TestBed.get(RemoteAssetService);
    expect(service).toBeTruthy();
  });
});
