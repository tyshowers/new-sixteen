import { TestBed } from '@angular/core/testing';

import { SixteenCustomerPrices1Service } from './sixteen-customer-prices1.service';

describe('SixteenCustomerPrices1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerPrices1Service = TestBed.get(SixteenCustomerPrices1Service);
    expect(service).toBeTruthy();
  });
});
