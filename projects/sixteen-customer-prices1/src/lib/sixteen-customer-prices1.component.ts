import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'sixteen-data';

@Component({
  selector: 'lib-sixteen-customer-prices1',
  templateUrl: './sixteen-customer-prices1.component.html',
  styles: []
})
export class SixteenCustomerPrices1Component implements OnInit {

  @Input() headingText = "Prices";
  @Input() descriptionText = "Aperiam eaque ipsa, quae ab illo inventore veritatis et expedita distinctio animi. Molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt.";
  @Input() products : Product[] = [];
  @Input() actionText = "Start Now";

  constructor() { }

  ngOnInit() {
  }

}
