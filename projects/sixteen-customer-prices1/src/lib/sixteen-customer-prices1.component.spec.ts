import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerPrices1Component } from './sixteen-customer-prices1.component';

describe('SixteenCustomerPrices1Component', () => {
  let component: SixteenCustomerPrices1Component;
  let fixture: ComponentFixture<SixteenCustomerPrices1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerPrices1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerPrices1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
