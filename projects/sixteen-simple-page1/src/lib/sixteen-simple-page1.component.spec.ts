import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenSimplePage1Component } from './sixteen-simple-page1.component';

describe('SixteenSimplePage1Component', () => {
  let component: SixteenSimplePage1Component;
  let fixture: ComponentFixture<SixteenSimplePage1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenSimplePage1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenSimplePage1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
