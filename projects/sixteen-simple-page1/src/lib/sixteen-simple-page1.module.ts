import { NgModule } from '@angular/core';
import { SixteenSimplePage1Component } from './sixteen-simple-page1.component';

@NgModule({
  declarations: [SixteenSimplePage1Component],
  imports: [
  ],
  exports: [SixteenSimplePage1Component]
})
export class SixteenSimplePage1Module { }
