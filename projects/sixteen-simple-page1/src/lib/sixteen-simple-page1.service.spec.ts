import { TestBed } from '@angular/core/testing';

import { SixteenSimplePage1Service } from './sixteen-simple-page1.service';

describe('SixteenSimplePage1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenSimplePage1Service = TestBed.get(SixteenSimplePage1Service);
    expect(service).toBeTruthy();
  });
});
