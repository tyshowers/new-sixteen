import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerProductFeatures4Component } from './sixteen-customer-product-features4.component';

@NgModule({
  declarations: [SixteenCustomerProductFeatures4Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerProductFeatures4Component]
})
export class SixteenCustomerProductFeatures4Module { }
