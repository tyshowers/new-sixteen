import { TestBed } from '@angular/core/testing';

import { SixteenCustomerProductFeatures4Service } from './sixteen-customer-product-features4.service';

describe('SixteenCustomerProductFeatures4Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerProductFeatures4Service = TestBed.get(SixteenCustomerProductFeatures4Service);
    expect(service).toBeTruthy();
  });
});
