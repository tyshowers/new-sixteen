import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerProductFeatures4Component } from './sixteen-customer-product-features4.component';

describe('SixteenCustomerProductFeatures4Component', () => {
  let component: SixteenCustomerProductFeatures4Component;
  let fixture: ComponentFixture<SixteenCustomerProductFeatures4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerProductFeatures4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerProductFeatures4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
