import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SixteenCustomerNewsletter1Component } from './sixteen-customer-newsletter1.component';

@NgModule({
  declarations: [SixteenCustomerNewsletter1Component],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule
  ],
  exports: [SixteenCustomerNewsletter1Component]
})
export class SixteenCustomerNewsletter1Module { }
