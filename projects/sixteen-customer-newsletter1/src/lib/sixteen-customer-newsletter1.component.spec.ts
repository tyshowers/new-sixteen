import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerNewsletter1Component } from './sixteen-customer-newsletter1.component';

describe('SixteenCustomerNewsletter1Component', () => {
  let component: SixteenCustomerNewsletter1Component;
  let fixture: ComponentFixture<SixteenCustomerNewsletter1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerNewsletter1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerNewsletter1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
