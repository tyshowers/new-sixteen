import { TestBed } from '@angular/core/testing';

import { SixteenCustomerNewsletter1Service } from './sixteen-customer-newsletter1.service';

describe('SixteenCustomerNewsletter1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerNewsletter1Service = TestBed.get(SixteenCustomerNewsletter1Service);
    expect(service).toBeTruthy();
  });
});
