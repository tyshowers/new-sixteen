/*
 * Public API Surface of sixteen-customer-newsletter1
 */

export * from './lib/sixteen-customer-newsletter1.service';
export * from './lib/sixteen-customer-newsletter1.component';
export * from './lib/sixteen-customer-newsletter1.module';
