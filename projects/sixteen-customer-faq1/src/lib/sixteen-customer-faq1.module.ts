import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerFaq1Component } from './sixteen-customer-faq1.component';

@NgModule({
  declarations: [SixteenCustomerFaq1Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerFaq1Component]
})
export class SixteenCustomerFaq1Module { }
