import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerFaq1Component } from './sixteen-customer-faq1.component';

describe('SixteenCustomerFaq1Component', () => {
  let component: SixteenCustomerFaq1Component;
  let fixture: ComponentFixture<SixteenCustomerFaq1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerFaq1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerFaq1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
