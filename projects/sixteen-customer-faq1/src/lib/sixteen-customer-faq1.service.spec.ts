import { TestBed } from '@angular/core/testing';

import { SixteenCustomerFaq1Service } from './sixteen-customer-faq1.service';

describe('SixteenCustomerFaq1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerFaq1Service = TestBed.get(SixteenCustomerFaq1Service);
    expect(service).toBeTruthy();
  });
});
