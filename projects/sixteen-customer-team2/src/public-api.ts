/*
 * Public API Surface of sixteen-customer-team2
 */

export * from './lib/sixteen-customer-team2.service';
export * from './lib/sixteen-customer-team2.component';
export * from './lib/sixteen-customer-team2.module';
