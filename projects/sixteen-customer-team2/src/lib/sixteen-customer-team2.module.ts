import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerTeam2Component } from './sixteen-customer-team2.component';
import { SixteenDataModule } from 'sixteen-data';

@NgModule({
  declarations: [SixteenCustomerTeam2Component],
  imports: [
    CommonModule,
    RouterModule,
    SixteenDataModule
  ],
  exports: [SixteenCustomerTeam2Component]
})
export class SixteenCustomerTeam2Module { }
