import { TestBed } from '@angular/core/testing';

import { SixteenCustomerTeam2Service } from './sixteen-customer-team2.service';

describe('SixteenCustomerTeam2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerTeam2Service = TestBed.get(SixteenCustomerTeam2Service);
    expect(service).toBeTruthy();
  });
});
