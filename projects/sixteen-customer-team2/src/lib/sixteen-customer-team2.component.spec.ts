import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerTeam2Component } from './sixteen-customer-team2.component';

describe('SixteenCustomerTeam2Component', () => {
  let component: SixteenCustomerTeam2Component;
  let fixture: ComponentFixture<SixteenCustomerTeam2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerTeam2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerTeam2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
