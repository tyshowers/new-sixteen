import { TestBed } from '@angular/core/testing';

import { SixteenCustomerIntegration1Service } from './sixteen-customer-integration1.service';

describe('SixteenCustomerIntegration1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerIntegration1Service = TestBed.get(SixteenCustomerIntegration1Service);
    expect(service).toBeTruthy();
  });
});
