import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerIntegration1Component } from './sixteen-customer-integration1.component';

@NgModule({
  declarations: [SixteenCustomerIntegration1Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerIntegration1Component]
})
export class SixteenCustomerIntegration1Module { }
