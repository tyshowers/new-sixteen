import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerIntegration1Component } from './sixteen-customer-integration1.component';

describe('SixteenCustomerIntegration1Component', () => {
  let component: SixteenCustomerIntegration1Component;
  let fixture: ComponentFixture<SixteenCustomerIntegration1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerIntegration1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerIntegration1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
