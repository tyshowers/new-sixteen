import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lib-sixteen-footer',
  templateUrl: './sixteen-footer.component.html',
  styleUrls: ['./sixteen-footer.component.css']
})
export class SixteenFooterComponent implements OnInit {

  @Input() copyrightText = "&#169; Copyright Taliferro IT Consulting. Taliferro &#174; Is Registered In The U.S. Patent And Trademark Office.";
  @Input() buttonLink = "https://www.taliferro.com";
  @Input() buttonText = "About the 16AHEAD Creator.";

  constructor() { }

  ngOnInit() {
  }

}
