import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenFooterComponent } from './sixteen-footer.component';

describe('SixteenFooterComponent', () => {
  let component: SixteenFooterComponent;
  let fixture: ComponentFixture<SixteenFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
