import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenFooterComponent } from './sixteen-footer.component';

@NgModule({
  declarations: [SixteenFooterComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenFooterComponent]
})
export class SixteenFooterModule { }
