import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenSimpleAboutDetails1Component } from './sixteen-simple-about-details1.component';

@NgModule({
  declarations: [SixteenSimpleAboutDetails1Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenSimpleAboutDetails1Component]
})
export class SixteenSimpleAboutDetails1Module { }
