import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenSimpleAboutDetails1Component } from './sixteen-simple-about-details1.component';

describe('SixteenSimpleAboutDetails1Component', () => {
  let component: SixteenSimpleAboutDetails1Component;
  let fixture: ComponentFixture<SixteenSimpleAboutDetails1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenSimpleAboutDetails1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenSimpleAboutDetails1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
