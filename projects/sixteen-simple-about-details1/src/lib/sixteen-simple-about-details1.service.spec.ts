import { TestBed } from '@angular/core/testing';

import { SixteenSimpleAboutDetails1Service } from './sixteen-simple-about-details1.service';

describe('SixteenSimpleAboutDetails1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenSimpleAboutDetails1Service = TestBed.get(SixteenSimpleAboutDetails1Service);
    expect(service).toBeTruthy();
  });
});
