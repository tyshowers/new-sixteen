import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NavComponent } from './nav/nav.component';
import { SixteenMenuModule } from 'sixteen-menu';

@NgModule({
  declarations: [NavComponent],
  imports: [
    CommonModule,
    RouterModule,
    SixteenMenuModule
  ],
  exports: [ NavComponent ],

})
export class SharedModule { }
