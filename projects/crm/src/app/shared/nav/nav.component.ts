import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  @Input() loggedIn = false;
  contact_id = 1;
  address_id = 1;
  email_address_id = 1;
  fop_id = 1;
  note_id = 1;
  phone_number_id = 1;


  constructor() { }

  ngOnInit() {
  }

}
