import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactRoutingModule } from './contact-routing.module';

import { ContactEditComponent } from './contact-edit/contact-edit.component';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactViewComponent } from './contact-view/contact-view.component';
import { ContactDashboardComponent } from './contact-dashboard/contact-dashboard.component';


@NgModule({
  declarations: [ContactEditComponent, ContactListComponent, ContactViewComponent, ContactDashboardComponent],
  imports: [
    CommonModule, ContactRoutingModule
  ]
})
export class ContactModule { }
