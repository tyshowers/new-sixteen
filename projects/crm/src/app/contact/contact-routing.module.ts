import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactEditComponent } from './contact-edit/contact-edit.component';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactViewComponent } from './contact-view/contact-view.component';
import { ContactDashboardComponent } from './contact-dashboard/contact-dashboard.component';

const ROUTES: Routes = [
  {
    path: '',
    children: [
      { path: '', component: ContactListComponent, data: { title: 'Contact List', state: 'contact list' } },
      { path: 'dashboard', component: ContactDashboardComponent, data: { title: 'Contact Dashboard', state: 'contact dashboard' } },
      { path: 'new', component: ContactEditComponent, data: { title: 'New Contact', state: 'new contact' } },
      { path: ':id', component: ContactViewComponent, data: { title: 'Contact Detail', state: 'contact detail' } },
      { path: ':id/edit', component: ContactEditComponent, data: { title: 'Edit Contact', state: 'edit contact' } },
    ]
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(ROUTES),
  ],
  exports: [
    RouterModule
  ],
  providers: []
})
export class ContactRoutingModule { }
