import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactModule } from './contact/contact.module';
import { AddressModule } from './address/address.module';
import { EmailAddressModule } from './email-address/email-address.module';
import { FopModule} from './fop/fop.module';
import { PhoneNumberModule } from './phone-number/phone-number.module';
import { NoteModule } from './note/note.module';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent, data:{ title: 'Contact Home'}, pathMatch: 'full'},
  { path: 'contacts', children: [ {path: '', loadChildren: './contact/contact.module#ContactModule'} ]},
  { path: 'contacts/:id/addresses', children: [ {path: '', loadChildren: './address/address.module#AddressModule'} ]},
  { path: 'contacts/:id/email-addresses', children: [ {path: '', loadChildren: './email-address/email-address.module#EmailAddressModule'} ]},
  { path: 'contacts/:id/form-of-payments', children: [ {path: '', loadChildren: './fop/fop.module#FopModule'} ]},
  { path: 'contacts/:id/notes', children: [ {path: '', loadChildren: './note/note.module#NoteModule'} ]},
  { path: 'contacts/:id/phone-numbers', children: [ {path: '', loadChildren: './phone-number/phone-number.module#PhoneNumberModule'} ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
