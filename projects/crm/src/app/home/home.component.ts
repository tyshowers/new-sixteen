import { Component, OnInit } from '@angular/core';
import { Carousel } from 'sixteen-data';
import { Article, Product, Review } from 'sixteen-data';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  // Heading
  header_headingText = 'Contact Management';
  header_descriptionText = 'Unlike any other contact management software you have used before';
  header_button1Text = 'Sign Up';
  header_button1Link;
  header_button2Text = 'Read More';
  header_button2Link;
  header_imageSrc = 'assets/contact-management-2.jpg';
  header_featureBox1 = 'Contact Suggestions';
  header_featureIcon1 = 'icon-people';
  header_badgeFeature1;
  header_featureBox2 = 'Contact Dashboard';
  header_badgeFeature2;
  header_featureIcon2 = 'icon-settings';
  // About 1
  about1_HeaderText = 'Unlimited Service Capabilities';
  about1_descriptionText = "Understand the value of your contacts. From their buying history with you to the last time you interacted with the contact.";
  about1_buttonText;
  about1_buttonLink;
  about1_featureBlockText1 = 'Contacts in the News';
  about1_featureBlockIcon1 = 'icon-notebook';

  about1_featureBlockText2 = 'Suggested Contacts';
  about1_featureBlockIcon2 = 'icon-phone';

  about1_featureBlockText3 = 'Purchase History';
  about1_featureBlockIcon3 = 'icon-clock';
  about1_featureBlockBadgeText3;

  about1_featureBlockText4 = 'Number of Interactions';
  about1_featureBlockIcon4 = 'icon-chart';
  about1_featureBlockBadgeText4;
  //About 2
  about2_imageURL1 = "assets/contact-management-4.jpg";
  about2_imageURL2 = "assets/contact-management-3.jpg";
  about2_headingText = "Reach Out";
  about2_descriptionText = "Based on our proprietary algorithm, 16AHEAD suggests who you should be contacting.";
  about2_bulletText1 = "Contact Value";
  about2_bulletText2 = "Contact Negligence";
  about2_bulletText3 = "Contacts Featured";
  about2_buttonText = "Read More";
  about2_buttonLink;

  carouselItems : Carousel[] = [];
  articles: Article[] = [];
  products: Product[] = [];
  reviews: Review[] = [];

  constructor() { }

  ngOnInit() {
    this.createReviews();
  }

  private createCarouselItems() : void {
    this.addToCarousel(this.getCarouselItem('1','Text 1','Some description 1','assets/1.jpg','/article/1','Learn More', 'Arial', 'Center'));
    this.addToCarousel(this.getCarouselItem('2','Text 2','Some description 2','assets/2.jpg','/article/2','Learn More', 'Arial', 'Center'));
    this.addToCarousel(this.getCarouselItem('3','Text 3','Some description 3','assets/3.jpg','/article/3','Learn More', 'Arial', 'Center'));
  }

  private createArticles() : void {
    this.addArticle(this.getArticle('1', 'Title 1', 'assets/1.jpg', '/articles/1', 'Article text 1', 'Article description 1', 'Love, bite, trees'));
    this.addArticle(this.getArticle('2', 'Title 2', 'assets/2.jpg', '/articles/2', 'Article text 2', 'Article description 2'));
    this.addArticle(this.getArticle('3', 'Title 3', 'assets/3.jpg', '/articles/3', 'Article text 3', 'Article description 3'));
  }

  private createProducts() : void {
    this.addProduct(this.getProduct('1', 'Product 1', 'Product description 1', 'assets/1.jpg', 18.5, 'category 1', 'manufacturer 1', 'author 1', 1, 'month'))
    this.addProduct(this.getProduct('2', 'Product 2', 'Product description 2', 'assets/2.jpg', 28.5, 'category 2', 'manufacturer 2', 'author 2', 2, 'month'))
    this.addProduct(this.getProduct('3', 'Product 3', 'Product description 3', 'assets/3.jpg', 38.5, 'category 3', 'manufacturer 3', 'author 3', 3, 'month'))
  }

  private createReviews() : void {
    this.addReview(this.getReview('1', [true, true, true, true, false], 'Description 1', 'User 1', 'User Company 1', 'assets/1.jpg'));
    this.addReview(this.getReview('2', [true, true, true, false, false], 'Description 2', 'User 2', 'User Company 2', 'assets/2.jpg', 'CEO'));
    this.addReview(this.getReview('3', [true, true, true, true, true], 'Description 3', 'User 3', 'User Company 3', 'assets/3.jpg'));
  }

  private getCarouselItem(id, title, description, url, link, linkText, textFont, textPosition) : Carousel {
    let item = new Carousel();
    item.id = id;
    item.title = title;
    item.description = description;
    item.url = url;
    item.link = link;
    item.linkText = linkText;
    item.textFont = textFont;
    item.textPosition = textPosition;
    return item;
  }

  private getProduct(id, name, description, url, price, category, manufacturer, author, orderQuantity, orderQuantityMeasure) : Product {
    let product = new Product(name);
    product.id = id;
    product.description = description;
    product.url = url;
    product.price = price;
    product.category = category;
    product.manufacturer = manufacturer;
    product.author = author;
    product.orderQuantity = orderQuantity;
    product.orderQuantityMeasure = orderQuantityMeasure;
    return product;
  }

  private getArticle(id, title, url, link, articleText, description, keywords?) : Article {
    let article = new Article();
    article.id = id;
    article.title= title;
    article.url = url;
    article.link = link;
    article.articleText = articleText;
    article.description = description;
    article.keywords = keywords;
    return article;
  }

  private getReview(id, stars, description, userName, userCompany, avatar, profession?) : Review {
    let review = new Review();
    review.id = id;
    review.stars = stars;
    review.description = description;
    review.userName = userName;
    review.userCompany = userCompany;
    review.avatar = avatar;
    review.profession = profession;
    return review;
  }

  private addToCarousel(carouselItem: Carousel) : void {
    this.carouselItems.push(carouselItem);
  }

  private addArticle(article: Article) : void {
    this.articles.push(article);
  }

  private addProduct(product: Product) : void {
    this.products.push(product);
  }

  private addReview(review: Review) : void {
    this.reviews.push(review);
  }

}
