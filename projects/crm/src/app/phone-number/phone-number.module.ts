import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { PhoneNumberRoutingModule } from './phone-number-routing.module';

import { PhoneNumberViewComponent } from './phone-number-view/phone-number-view.component';
import { PhoneNumberListComponent } from './phone-number-list/phone-number-list.component';
import { PhoneNumberEditComponent } from './phone-number-edit/phone-number-edit.component';

@NgModule({
  declarations: [PhoneNumberViewComponent, PhoneNumberListComponent, PhoneNumberEditComponent ],
  imports: [
    CommonModule, PhoneNumberRoutingModule, SharedModule
  ]
})
export class PhoneNumberModule { }
