import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { NoteRoutingModule } from './note-routing.module';
import { NoteListComponent } from './note-list/note-list.component';
import { NoteEditComponent } from './note-edit/note-edit.component';
import { NoteViewComponent } from './note-view/note-view.component';


@NgModule({
  declarations: [NoteListComponent, NoteEditComponent, NoteViewComponent],
  imports: [
    CommonModule,
    NoteRoutingModule,
    SharedModule
  ]
})
export class NoteModule { }
