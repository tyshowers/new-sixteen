import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { AddressRoutingModule } from './address-routing.module';

import { AddressEditComponent } from './address-edit/address-edit.component';
import { AddressViewComponent } from './address-view/address-view.component';
import { AddressListComponent } from './address-list/address-list.component';

@NgModule({
  declarations: [AddressEditComponent, AddressViewComponent, AddressListComponent],
  imports: [
    CommonModule, AddressRoutingModule, SharedModule
  ]
})
export class AddressModule { }
