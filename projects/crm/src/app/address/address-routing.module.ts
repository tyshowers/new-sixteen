import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddressEditComponent } from './address-edit/address-edit.component';
import { AddressViewComponent } from './address-view/address-view.component';
import { AddressListComponent } from './address-list/address-list.component';

const ROUTES: Routes = [
  {
    path: '',
    children: [
      { path: '', component: AddressListComponent, data: { title: 'Address List', state: 'address list' } },
      { path: 'new', component: AddressEditComponent, data: { title: 'New Address', state: 'new address' } },
      { path: ':id', component: AddressViewComponent, data: { title: 'Address Detail', state: 'address detail' } },
      { path: ':id/edit', component: AddressEditComponent, data: { title: 'Edit Address', state: 'edit address' } },

    ]
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(ROUTES),
  ],
  exports: [
    RouterModule
  ],
  providers: []
})
export class AddressRoutingModule { }
