import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import { SixteenFooterModule } from 'sixteen-footer';
import { SixteenDataModule } from 'sixteen-data';
import { SixteenCustomerHeader1Module } from 'sixteen-customer-header1';
import { SixteenCustomerHeader2Module } from 'sixteen-customer-header2';
import { SixteenCustomerHeader3Module } from 'sixteen-customer-header3';
import { SixteenCustomerHeader4Module } from 'sixteen-customer-header4';
import { SixteenCustomerHeader5Module } from 'sixteen-customer-header5';

import { SixteenCarouselModule} from 'sixteen-carousel';

import { SixteenCustomerAboutProduct1Module } from 'sixteen-customer-about-product1';
import { SixteenCustomerAboutProduct2Module } from 'sixteen-customer-about-product2';
import { SixteenCustomerAboutProduct3Module } from 'sixteen-customer-about-product3';
import { SixteenCustomerAboutProduct4Module } from 'sixteen-customer-about-product4';
import { SixteenCustomerAboutProduct5Module } from 'sixteen-customer-about-product5';
import { SixteenCustomerAboutProduct6Module } from 'sixteen-customer-about-product6';
import { SixteenCustomerAboutProduct7Module } from 'sixteen-customer-about-product7';

import { SixteenCustomerContactUs1Module } from 'sixteen-customer-contact-us1';

import { SixteenCustomerFaq1Module } from 'sixteen-customer-faq1';
import { SixteenCustomerFaq2Module } from 'sixteen-customer-faq2';
import { SixteenCustomerFaq3Module } from 'sixteen-customer-faq3';

import { SixteenCustomerIntegration1Module } from 'sixteen-customer-integration1';
import { SixteenCustomerIntegration2Module } from 'sixteen-customer-integration2';

import { SixteenCustomerPrices1Module} from 'sixteen-customer-prices1';
import { SixteenCustomerPrices2Module} from 'sixteen-customer-prices2';

import { SixteenCustomerProductFeatures1Module } from 'sixteen-customer-product-features1';
import { SixteenCustomerProductFeatures2Module } from 'sixteen-customer-product-features2';
import { SixteenCustomerProductFeatures3Module } from 'sixteen-customer-product-features3';
import { SixteenCustomerProductFeatures4Module } from 'sixteen-customer-product-features4';
import { SixteenCustomerProductFeatures5Module } from 'sixteen-customer-product-features5';

import { SixteenCustomerHowTo3Module } from 'sixteen-customer-how-to3';
import { SixteenCustomerParallax1Module } from 'sixteen-customer-parallax1';

import { SixteenCustomerLatestPost1Module } from 'sixteen-customer-latest-post1';
import { SixteenCustomerLatestPost2Module } from 'sixteen-customer-latest-post2';
import { SixteenCustomerTryIt1Module } from 'sixteen-customer-try-it1';
import { SixteenCustomerTeam1Module} from 'sixteen-customer-team1';

import { SixteenCustomerNewsletter1Module } from 'sixteen-customer-newsletter1';
import { SixteenCustomerReviews1Module} from 'sixteen-customer-reviews1';
import { SixteenCustomerReviews2Module} from 'sixteen-customer-reviews2';
import { SixteenCustomerReviews3Module} from 'sixteen-customer-reviews3';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SixteenFooterModule,
    SixteenDataModule,
    SharedModule,
    SixteenCustomerHeader1Module,
    SixteenCustomerHeader2Module,
    SixteenCustomerHeader3Module,
    SixteenCustomerHeader4Module,
    SixteenCustomerHeader5Module,
    SixteenCustomerAboutProduct1Module,
    SixteenCustomerAboutProduct2Module,
    SixteenCustomerAboutProduct3Module,
    SixteenCustomerAboutProduct4Module,
    SixteenCustomerAboutProduct5Module,
    SixteenCustomerAboutProduct6Module,
    SixteenCustomerAboutProduct7Module,
    SixteenCustomerContactUs1Module,
    SixteenCustomerProductFeatures1Module,
    SixteenCustomerProductFeatures3Module,
    SixteenCustomerProductFeatures4Module,
    SixteenCustomerProductFeatures5Module,
    SixteenCustomerHowTo3Module,
    SixteenCustomerParallax1Module,
    SixteenCustomerFaq1Module,
    SixteenCustomerFaq2Module,
    SixteenCustomerFaq3Module,
    SixteenCustomerLatestPost1Module,
    SixteenCustomerLatestPost2Module,
    SixteenCustomerTryIt1Module,
    SixteenCustomerTeam1Module,
    SixteenCustomerProductFeatures2Module,
    SixteenCarouselModule,
    SixteenCustomerIntegration1Module,
    SixteenCustomerIntegration2Module,
    SixteenCustomerNewsletter1Module,
    SixteenCustomerPrices1Module,
    SixteenCustomerPrices2Module,
    SixteenCustomerReviews1Module,
    SixteenCustomerReviews2Module,
    SixteenCustomerReviews3Module
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

@NgModule({})
export class CRMModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AppModule,
      providers: []
    }

  }
}
