import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerReviews1Component } from './sixteen-customer-reviews1.component';
import { SixteenDataModule } from 'sixteen-data';

@NgModule({
  declarations: [SixteenCustomerReviews1Component],
  imports: [
    CommonModule,
    RouterModule,
    SixteenDataModule
  ],
  exports: [SixteenCustomerReviews1Component]
})
export class SixteenCustomerReviews1Module { }
