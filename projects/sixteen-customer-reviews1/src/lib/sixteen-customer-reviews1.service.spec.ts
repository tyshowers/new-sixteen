import { TestBed } from '@angular/core/testing';

import { SixteenCustomerReviews1Service } from './sixteen-customer-reviews1.service';

describe('SixteenCustomerReviews1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerReviews1Service = TestBed.get(SixteenCustomerReviews1Service);
    expect(service).toBeTruthy();
  });
});
