import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerReviews1Component } from './sixteen-customer-reviews1.component';

describe('SixteenCustomerReviews1Component', () => {
  let component: SixteenCustomerReviews1Component;
  let fixture: ComponentFixture<SixteenCustomerReviews1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerReviews1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerReviews1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
