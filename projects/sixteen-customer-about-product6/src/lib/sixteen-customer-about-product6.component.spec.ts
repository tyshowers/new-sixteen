import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerAboutProduct6Component } from './sixteen-customer-about-product6.component';

describe('SixteenCustomerAboutProduct6Component', () => {
  let component: SixteenCustomerAboutProduct6Component;
  let fixture: ComponentFixture<SixteenCustomerAboutProduct6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerAboutProduct6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerAboutProduct6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
