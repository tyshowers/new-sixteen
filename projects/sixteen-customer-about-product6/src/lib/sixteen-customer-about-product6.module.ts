import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerAboutProduct6Component } from './sixteen-customer-about-product6.component';

@NgModule({
  declarations: [SixteenCustomerAboutProduct6Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerAboutProduct6Component]
})
export class SixteenCustomerAboutProduct6Module { }
