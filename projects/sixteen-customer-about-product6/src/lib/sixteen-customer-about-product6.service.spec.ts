import { TestBed } from '@angular/core/testing';

import { SixteenCustomerAboutProduct6Service } from './sixteen-customer-about-product6.service';

describe('SixteenCustomerAboutProduct6Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerAboutProduct6Service = TestBed.get(SixteenCustomerAboutProduct6Service);
    expect(service).toBeTruthy();
  });
});
