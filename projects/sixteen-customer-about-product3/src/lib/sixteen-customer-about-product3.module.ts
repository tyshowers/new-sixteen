import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerAboutProduct3Component } from './sixteen-customer-about-product3.component';

@NgModule({
  declarations: [SixteenCustomerAboutProduct3Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerAboutProduct3Component]
})
export class SixteenCustomerAboutProduct3Module { }
