import { TestBed } from '@angular/core/testing';

import { SixteenCustomerAboutProduct3Service } from './sixteen-customer-about-product3.service';

describe('SixteenCustomerAboutProduct3Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerAboutProduct3Service = TestBed.get(SixteenCustomerAboutProduct3Service);
    expect(service).toBeTruthy();
  });
});
