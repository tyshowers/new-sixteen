import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerAboutProduct3Component } from './sixteen-customer-about-product3.component';

describe('SixteenCustomerAboutProduct3Component', () => {
  let component: SixteenCustomerAboutProduct3Component;
  let fixture: ComponentFixture<SixteenCustomerAboutProduct3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerAboutProduct3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerAboutProduct3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
