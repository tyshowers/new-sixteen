import { TestBed } from '@angular/core/testing';

import { SixteenCustomerTeam1Service } from './sixteen-customer-team1.service';

describe('SixteenCustomerTeam1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerTeam1Service = TestBed.get(SixteenCustomerTeam1Service);
    expect(service).toBeTruthy();
  });
});
