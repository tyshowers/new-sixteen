import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerTeam1Component } from './sixteen-customer-team1.component';

describe('SixteenCustomerTeam1Component', () => {
  let component: SixteenCustomerTeam1Component;
  let fixture: ComponentFixture<SixteenCustomerTeam1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerTeam1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerTeam1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
