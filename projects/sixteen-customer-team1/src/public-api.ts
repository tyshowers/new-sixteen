/*
 * Public API Surface of sixteen-customer-team1
 */

export * from './lib/sixteen-customer-team1.service';
export * from './lib/sixteen-customer-team1.component';
export * from './lib/sixteen-customer-team1.module';
