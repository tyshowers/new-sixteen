import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerProductFeatures3Component } from './sixteen-customer-product-features3.component';

@NgModule({
  declarations: [SixteenCustomerProductFeatures3Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerProductFeatures3Component]
})
export class SixteenCustomerProductFeatures3Module { }
