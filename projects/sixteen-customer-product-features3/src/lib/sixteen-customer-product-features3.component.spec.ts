import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerProductFeatures3Component } from './sixteen-customer-product-features3.component';

describe('SixteenCustomerProductFeatures3Component', () => {
  let component: SixteenCustomerProductFeatures3Component;
  let fixture: ComponentFixture<SixteenCustomerProductFeatures3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerProductFeatures3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerProductFeatures3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
