import { TestBed } from '@angular/core/testing';

import { SixteenCustomerProductFeatures3Service } from './sixteen-customer-product-features3.service';

describe('SixteenCustomerProductFeatures3Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerProductFeatures3Service = TestBed.get(SixteenCustomerProductFeatures3Service);
    expect(service).toBeTruthy();
  });
});
