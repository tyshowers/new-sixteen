import { IState } from './i-state';
/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Setting
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      10/12/2017  Baselined
*****************************************************************************/
export class Setting implements IState {
  public $key: string;
  public id = '';
  public name: string;
  public step: number;
  public description: string;
  public activated: boolean = false;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public user_id;
  public views: number;
  public lastViewed;
  public appSettings: AppSetting;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.name = (data.name) ? data.name : null;
      data.step = (data.step) ? data.step : null;
      data.description = (data.description) ? data.description : null;
      data.activiated = (data.activiated) ? true : false;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.lastUpdated = (data.lastUpdated) ? new Date(data.lastUpdated) : null;
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.draft = (data.draft) ? data.draft : false;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}

export interface AppSetting {
  blog?: boolean;
  contact?: boolean;
  document?: boolean;
  email?: boolean;
  project?: boolean;
  commerce?: boolean;
  messaging?: boolean;
  opportunity?: boolean;
  news?: boolean;
  help?: boolean;
  calendar?: boolean;
  timecard?: boolean;
  mylan?: boolean;
  favorite?: boolean;
  history?: boolean;
  concierge?: boolean;
}
