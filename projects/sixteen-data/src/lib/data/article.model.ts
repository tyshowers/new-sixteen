import { Image } from './image.model';

export class Article {
  public $key: string;
  public id: string;
  public title: string;
  public link: string;
  public pubDate;
  public creator:string;
  public blog_id: string;
  public _isPermaLink: string;
  public __text: string;
  public description: string;
  public articleText: string;
  public image: Image;
  public url: string;
  public keywords:string;
  public user_id;
  public lastUpdated;
  public lastUpdatedBy;
  public creatorName;
  public deleted: boolean;
  public draft: boolean;
  public views: number;
  public lastViewed;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.blog_id = (data.blog_id) ? data.blog_id : null;
      data.title = (data.title) ? data.title : null;
      data.url = (data.url) ? data.url : null;
      data.link = (data.link) ? data.link : null;
      data.pubDate = (data.pubDate) ? new Date(data.pubDate) : (new Date());
      data.creator = (data.creator) ? data.creator : null;
      data._isPermaLink = (data._isPermaLink) ? data._isPermaLink : null;
      data.__text = (data.__text) ? data.__text : null;
      data.description = (data.description) ? data.description : null;
      data.articleText = (data.articleText) ? data.articleText : null;
      data.image = (data.image) ? data.image : null;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.lastUpdated = (data.lastUpdated) ? new Date(data.lastUpdated) : null;
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.deleted =(data.deleted) ? data.deleted : false;
      data.draft = (data.draft) ? data.draft : false;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date( data.lastViewed) : (new Date());
  }
}
