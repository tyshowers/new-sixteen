/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: ProductBundle
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      09/20/2017  Baselined
*
*****************************************************************************/
export class ProductBundle {
  public $key: string;
  public id;
  public name;
  public description;
  public url;
  public products: Array<any> = [];
  public activated: boolean = false;
  public price: number;
  public hourlyRate: boolean = false;
  public catalog_id;
  public store_id;
  public lastUpdated;
  public lastUpdatedBy;
  public keywords: string;
  public creatorName;
  public deleted: boolean;
  public draft: boolean;
  public user_id;
  public views: number;
  public lastViewed;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.store_id = (data.store_id) ? data.store_id : null;
      data.name = (data.name) ? data.name : null;
      data.url = (data.url) ? data.url : null;
      data.description = (data.description) ? data.description : null;
      data.price = (data.price) ? data.price : 0;
      data.products = (data.products) ? data.products : [];
      data.activated = (data.activated) ? data.activated : false;
      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.keywords = (data.keywords) ? data.keywords : [];
      data.user_id = (data.user_id) ? data.user_id : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}
