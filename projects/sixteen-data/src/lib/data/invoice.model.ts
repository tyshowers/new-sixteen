import { Address } from './address.model';
import { IState } from './i-state';
/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Invoice
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      08/17/2017  Baselined
*  0.2      08/18/2017  Added state interface
*****************************************************************************/

export class Invoice implements IState {
  public $key: string;
  public id: string;
  public catalog_id;
  public contact_id;
  public order_id;
  public store_id;
  public status: string;
  public date;
  public shipDate;
  public shipper: string;
  public shipperTrackingNumber: string;
  public shippingCost;
  public shippingTax;
  public paid: boolean = false;
  public address: Address;
  public notes : string;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public user_id;
  public views: number;
  public lastViewed;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.order_id = (data.order_id) ? data.status : null;
      data.status = (data.status) ? data.message : null;
      data.contact_id = (data.contact_id) ? data.contact_id : null;
      data.store_id = (data.store_id) ? data.store_id : null;
      data.date = (data.date) ? new Date(data.date) : null;
      data.shipDate = (data.shipDate) ? new Date(data.shipDate) : null;
      data.shipper = (data.shipper) ? data.shipper : null;
      data.shipperTrackingNumber = (data.shipperTrackingNumber) ? data.shipperTrackingNumber : null;
      data.shippingCost = (data.shippingCost) ? data.shippingCost : null;
      data.paid = (data.paid) ? data.paid : false;
      data.address = (data.address) ? data.address : null;
      data.notes = (data.notes) ? data.notes : null;
      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}
