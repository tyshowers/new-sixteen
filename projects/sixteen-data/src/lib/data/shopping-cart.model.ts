import { Product } from './product.model';
/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: ShoppingCart
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      12/06/2017  Baselined
*  0.2      12/08/2017  Added indicator to determine if shipping is required
*****************************************************************************/

export class ShoppingCart {
  public $key: string;
  id;
  orderLine: IorderLine[];
  public store_id;
  public storeName;
  public contact_id;
  public address_id;
  public fop_id;
  public invoice_id;
  public shippingRequired: boolean = false;
  public productViewHistory = [];
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public user_id;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.orderLine = (data.orderLine) ? data.orderLine : [];
      data.store_id = (data.store_id) ? data.store_id : null;
      data.contact_id = (data.contact_id) ? data.contact_id : null;
      data.address_id = (data.address_id) ? data.address_id : null;
      data.fop_id = (data.fop_id) ? data.fop_id : null;
      data.invoice_id = (data.invoice_id) ? data.invoice_id : null;
      data.shippingRequired = (data.shippingRequired) ? data.shippingRequired : false;
      data.productViewHistory = (data.productViewHistory) ? data.productViewHistory : [];
      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
  }
}

export interface IorderLine {
  quantity: number;
  product: Product;

}
