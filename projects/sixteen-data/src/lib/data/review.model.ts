export class Review  {
  public $key: string;
  public id;
  public stars = [];
  public description;
  public userName;
  public profession;
  public userCompany;
  public avatar;

}
