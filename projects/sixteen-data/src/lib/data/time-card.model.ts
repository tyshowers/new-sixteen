/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: TimeCard Format
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      08/17/2017  Baselined
*
*****************************************************************************/
export class TimeCard {
  public $key: string;
  public id;
  public project_id;
  public date;
  public hours;
  public user_id;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.project_id = (data.project_id) ? data.project_id : null;
      data.date = (data.date) ? new Date(data.date) : null;
      data.hours = (data.hours) ? data.hours : null;
      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
  }
}
