import { IState } from './i-state';
import { IDropdown }from './i-dropdown';
/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Help
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      08/17/2017  Baselined
*  0.2      08/18/2017  Added state and dropdown interface
*****************************************************************************/
export class Help implements IState, IDropdown {
  public $key: string;
  public id : string;
  public name : string;
  public description : string;
  public author: string;
  public step;
  public url;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public user_id;
  public views: number;
  public lastViewed;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.name = (data.name) ? data.name : null;
      data.url = (data.url) ? data.url : null;
      data.description = (data.description) ? data.description : null;
      data.author = (data.author) ? data.author : null;
      data.status = (data.status) ? data.status : null;
      data.step = (data.step) ? data.step : null;
      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}

export class HelpProgress implements IState {
  public $key: string;
  public id : string;
  public help_id: string;
  public url;
  public itemToComplete;
  public status: string;
  public complete: boolean = false;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public user_id;
  public views: number;
  public lastViewed;

}
