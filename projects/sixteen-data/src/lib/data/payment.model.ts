import { IState } from './i-state';
/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Payment
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      08/17/2017  Baselined
*  0.2      08/18/2017  Added state interface
*****************************************************************************/

export class Payment implements IState {
  public $key: string;
  public id;
  public contact_id;
  public company_id;
  public catalog_id;
  public store_id;
  public status: string;
  public authorizationAmount: number;
  public requestAmout: number;
  public confirmationNumber: string;
  public processingTime: string;
  public date;
  public paymentMethodCode: string;
  public paymentLine: PaymentLine;
  public paymentAttachment: PaymentAttachment;
  public fromBankAccount: FromBankAccount;
  public check: Check;
  public paypal: Paypal;
  public approvalCode: string;
  public creditCardPayment: CardPayment;
  public merchantId: string;
  public paymentChannelCode: string;
  public paymentSource: string;
  public paymentSchedulingType: string;
  public transactionType: string;
  public fopType: string;
  public amount: number = 0;
  public reversalIndicator: boolean = false;
  public authorizationChannel: string;
  public pointOfSaleReceiptNumber: string;
  public manualPaymentIndicator: boolean = false;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public user_id;
  public views: number;
  public lastViewed;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.store_id = (data.store_id) ? data.store_id : null;
      data.catalog_id = (data.catalog_id) ? data.catalog_id : null;
      data.status = (data.status) ? data.status : null;
      data.contact_id = (data.contact_id) ? data.contact_id : null;
      data.company_id = (data.company_id) ? data.company_id : null;
      data.authorizationAmount = (data.authorizationAmount) ? data.authorizationAmount : 0;
      data.requestAmout = (data.requestAmout) ? data.requestAmout : 0;
      data.confirmationNumber = (data.confirmationNumber) ? data.confirmationNumber : null;
      data.processingTime = (data.processingTime) ? data.processingTime : null;
      data.date = (data.date) ? new Date(data.date) : null;
      data.paymentMethodCode = (data.paymentMethodCode) ? data.paymentMethodCode : null;
      data.paymentLine = (data.paymentLine) ? data.paymentLine : null;
      data.paymentAttachment = (data.paymentAttachment) ? data.paymentAttachment : null;
      data.fromBankAccount = (data.fromBankAccount) ? data.fromBankAccount : null;
      data.paypal = (data.paypal) ? data.paypal : null;
      data.check = (data.check) ? data.check : null;
      data.approvalCode = (data.approvalCode) ? data.approvalCode : null;
      data.creditCardPayment = (data.creditCardPayment) ? data.creditCardPayment : null;
      data.merchantId = (data.merchantId) ? data.merchantId : null;
      data.paymentChannelCode = (data.paymentChannelCode) ? data.paymentChannelCode : null;
      data.paymentSource = (data.paymentSource) ? data.paymentSource : null;
      data.paymentSchedulingType = (data.paymentSchedulingType) ? data.paymentSchedulingType : null;
      data.transactionType = (data.transactionType) ? data.transactionType : null;
      data.fopType = (data.fopType) ? data.fopType : null;
      data.amount = (data.amount) ? data.amount : 0;
      data.reversalIndicator = (data.reversalIndicator) ? data.reversalIndicator : null;
      data.authorizationChannel = (data.authorizationChannel) ? data.authorizationChannel : null;
      data.pointOfSaleReceiptNumber = (data.pointOfSaleReceiptNumber) ? data.pointOfSaleReceiptNumber : null;
      data.manualPaymentIndicator = (data.manualPaymentIndicator) ? data.manualPaymentIndicator : null;
      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}

export class Paypal {
  public $key: string;
  id = '';
}

export class PaymentLine {
  public $key: string;
  id = '';
  creditIndicator: boolean = false;
  allocationTypeCode: string;
  allocationAmount: number;
  description: string;
  financialAccountNumber: string;
}

export class PaymentAttachment {
  public $key: string;
  id;
  typeCode: string;
  name: string;
  title: string;
  URI: string;
  description: string;
}

export class FromBankAccount {
  public $key: string;
  id;
  accountNumber: number = 0;
  bankName: string;
  routingNumber: number = 0;
}

export class Check {
  public $key: string;
  id;
  accountHolderName: string;
  bankName: string;
  accountNumber: number = 0;
  routingNumber: number = 0;
  checkNumber: number = 0;
}
export class CardPayment {
  public $key: string;
  id;
  traceNumber: string;
  cardType: string;
  cardNumber: number = 0;
  cardHolderName: string;
  expirationMonthYear: string;
}
