import { IState } from './i-state';
/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Alert
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      11/22/2017  Baselined
*****************************************************************************/
export class Docuttach implements IState {
  $key: string;
  public id: string;
  public name: string;
  public url: string;
  public isImage: boolean;
  public contact_id: string;
  public project_id;
  public order_id;
  public event_id;
  public message_id;
  public product_id;
  public catalog_id;
  public store_id;
  public doc;
  public lastUpdated;
  public lastUpdatedBy;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords:string;
  public user_id;
  public views: number;
  public lastViewed;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : data.$key;
      data.name = (data.name) ? data.name : null;
      data.url = (data.url) ? data.url : null;
      data.contact_id = (data.contact_id) ? data.contact_id : null;
      data.project_id = (data.project_id) ? data.project_id : null;
      data.order_id = (data.order_id) ? data.order_id : null;
      data.help_id = (data.help_id) ? data.help_id : null;
      data.event_id = (data.event_id) ? data.event_id : null;
      data.product_bundle_id = (data.product_bundle_id) ? data.product_bundle_id : null;
      data.message_id = (data.message_id) ? data.message_id : null;
      data.product_id = (data.product_id) ? data.product_id : null;
      data.catalog_id = (data.catalog_id) ? data.catalog_id : null;
      data.offer_id = (data.offer_id) ? data.offer_id : null;
      data.store_id = (data.store_id) ? data.store_id : null;
      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}
