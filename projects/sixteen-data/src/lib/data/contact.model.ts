import { IDropdown } from './i-dropdown';
import { IState } from './i-state';
/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Contact
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      08/17/2017  Baselined
*  0.2      08/18/2017  Added state and dropdown interface
*  0.3      10/21/2017  Removed constructors
*****************************************************************************/

export class Contact implements IState {
  public $key: string;
  public id : string;
  public firstName: string;
  public middleName: string;
  public lastName: string;
  public company_id: string;
  public isCompany: boolean = false;
  public ssn: string;
  public company: Company = new Company();
  public prefix : string;
  public url : string;
  public profession : string;
  public status : string;
  public profileType : string;
  public linkedinUrl : string;
  public nickname : string;
  public birthday : string;
  public anniversary: any = null;
  public gender : string;
  public dependents: Array<any> = [];
  public preferences: Array<any> = [];
  public opportunities: Array<any> = [];
  public orders: Array<any> = [];
  public FOPs: Array<any> = [];
  public events: Array<any> = [];
  public alerts: Array<any> = [];
  public projects: Array<any> = [];
  public invoices: Array<any> = [];
  public ratings: Array<any> = [];
  public images: Array<any> = [];
  public phoneNumbers: Array<any> = [];
  public emailAddresses: Array<any> = [];
  public tempScore: number = 0;
  public addresses: Array<any> = [];
  public notes: Array<any> = [];
  public shared: boolean = false;
  public systemUser: boolean = false;
  public employee: boolean = false;
  public billingRate: number = 0;
  public loginID: string;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public user_id;
  public keywords:string;
  public app_user_id: string;
  public views: number;
  public lastViewed;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.firstName = (data.firstName) ? data.firstName : null;
      data.middleName = (data.middleName) ? data.middleName : null;
      data.lastName = (data.lastName) ? data.lastName : null;
      data.ssn = (data.ssn) ? data.ssn : null;
      data.company_id = (data.company_id) ? data.company_id : null;
      data.company = (data.company) ? data.company : new Company();
      data.name = (data.name) ? data.name : null;
      data.prefix = (data.prefix) ? data.prefix : null;
      data.url = (data.url) ? data.url : null;
      data.profession = (data.profession) ? data.profession : null;
      data.status = (data.status) ? data.status : null;
      data.profileType = (data.profileType) ? data.profileType : null;
      data.linkedinUrl = (data.linkedinUrl) ? data.linkedinUrl : null;
      data.nickname = (data.nickname) ? data.nickname : null;
      data.birthday = (data.birthday) ? data.birthday : null;
      data.anniversary = (data.anniversary) ? data.anniversary : null;
      data.gender = (data.gender) ? data.gender : null;
      data.preferences = (data.preferences) ? data.preferences : [];
      data.opportunities = (data.opportunities) ? data.opportunities : [];
      data.dependents = (data.dependents) ? data.dependents : [];
      data.orders = (data.orders) ? data.orders : [];
      data.FOPs = (data.FOPs) ? data.FOPs : [];
      data.events = (data.events) ? data.events : [];
      data.alerts = (data.alerts) ? data.alerts : [];
      data.projects = (data.projects) ? data.projects : [];
      data.invoices = (data.invoices) ? data.invoices : [];
      data.ratings = (data.ratings) ? data.ratings : [];
      data.images = (data.images) ? data.images : [];
      data.phoneNumbers = (data.phoneNumbers) ? data.phoneNumbers : [];
      data.emailAddresses = (data.emailAddresses) ? data.emailAddresses : [];
      data.addresses = (data.addresses) ? data.addresses : [];
      data.notes = (data.notes) ? data.notes : [];
      data.shared = (data.shared) ? data.shared : false;
      data.systemUser = (data.systemUser) ? data.systemUser : false;
      data.employee = (data.employee) ? data.employee : false;
      data.billingRate = (data.billingRate) ? data.billingRate : 0;
      data.loginID = (data.loginID) ? data.loginID : null;
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.app_user_id = (data.app_user_id) ? data.app_user_id : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}

export class Dependent {
  public firstName;
  public lastName;
  public relationship;
}

export class Company implements IDropdown, IState {
  public id : string;
  public name: string
  public numberOfEmployees : string;
  public other : string;
  public phoneNumbers: Array<any> = [];
  public emailAddresses: Array<any> = [];
  public addresses: Array<any> = [];
  public url : string;
  public sicCode : string;
  public status : string;
  public shared: boolean = false;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords:string;
  public user_id;
  public views: number;
  public lastViewed;

}
