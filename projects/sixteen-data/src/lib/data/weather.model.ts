/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Weather
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      08/27/2017  Baselined
*****************************************************************************/
export class Weather {
  public cityName: string
  public country: string;
  zip: string;
  coord: Coordinates;
  weatherDescription: WeatherDescription;
  base: string;
  main: WeatherMain;
  visibility: number;
  wind: Wind;
  clouds: Clouds;
  dt: number;
  sys: Sys;
  id: string;
  name: string;
  cod: number;

  constructor() { }
}

export class Coordinates {
  lon: number;
  lat: number;
}
export class WeatherDescription {
  id: number;
  main: string;
  description: string;
  icon: string;
}
export class WeatherMain {
  temp: number;
  pressure: number;
  humidity: number;
  temp_min: number;
  temp_max: number;
}
export class Wind {
  speed: number;
}
export class Clouds {
  all: number;
}
export class Sys {
  type: number;
  id: number;
  message: number;
  country: number;
  sunrise: number;
  sunset: number;
}
