export class Goal {

  public $key: string;
  public id : string;
  public goalYear: number;
  public description: string;
  public revenueGoal : TimeGoal;
  public contactGoal : TimeGoal;
  public orderGoal : TimeGoal;
  public eventGoal : TimeGoal;
  public projectGoal : TimeGoal;
  public blogGoal : TimeGoal;
  public messageGoal : TimeGoal;
  public taskGoal : TimeGoal;
  public paymentGoal : TimeGoal;
  public opportunityGoal : TimeGoal;
  public shoppingCartGoal : TimeGoal;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public user_id;
  public views: number;
  public lastViewed;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.goalYear = (data.goalYear) ? data.goalYear : null;
      data.description = (data.description) ? data.description : null;
      data.revenueGoal = (data.revenueGoal) ? data.revenueGoal : null;
      data.contactGoal = (data.contactGoal) ? data.contactGoal : null;
      data.orderGoal = (data.orderGoal) ? data.orderGoal : null;
      data.eventGoal = (data.eventGoal) ? data.eventGoal : null;
      data.projectGoal = (data.projectGoal) ? data.projectGoal : null;
      data.blogGoal = (data.blogGoal) ? data.blogGoal : null;
      data.taskGoal = (data.taskGoal) ? data.taskGoal : null;
      data.paymentGoal = (data.paymentGoal) ? data.paymentGoal : null;
      data.opportunityGoal = (data.opportunityGoal) ? data.opportunityGoal : null;
      data.shoppingCartGoal = (data.shoppingCartGoal) ? data.shoppingCartGoal : null;

      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}

export class TimeGoal {

  constructor(public oneMonth : number,
  public threeMonth : number,
  public sixMonth: number,
  public year: number) {}

}
