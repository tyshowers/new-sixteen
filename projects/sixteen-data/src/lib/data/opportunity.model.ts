import { IDropdown } from './i-dropdown';
import { IState } from './i-state';
/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Opportunity
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      08/17/2017  Baselined
*  0.2      08/18/2017  Added state and interface
*****************************************************************************/

export class Opportunity implements IState, IDropdown {
  public $key: string;
  public id: string;
  public name: string;
  public opportunityType: string;
  public department_id: string;
  public supplier_id: string;
  public currentStage: string;
  public dueDate;
  public status: string; //set on creation
  public source: string; //set on creation e.g., email or contact creation if client type chosen
  public nextStep: string; //set on creation
  public campaign: string ;
  public probability: string; //set on creation and degrade with time
  public otherAmount: number;
  public notes: string;
  public createDate: string; //set at creation
  public dollarAmount: number;
  public contact_id: string;
  public contactName: string;
  public company_id: string;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public user_id;
  public views: number;
  public lastViewed;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.name = (data.name) ? data.name : null;
      data.opportunityType = (data.opportunityType) ? data.opportunityType : null;
      data.currentStage = (data.currentStage) ? data.currentStage : null;
      data.dueDate = (data.dueDate) ? new Date(data.dueDate) : null;
      data.status = (data.status) ? data.status : null;
      data.source = (data.source) ? data.source : null;
      data.nextStep = (data.nextStep) ? data.nextStep : null;
      data.campaign = (data.campaign) ? data.campaign : null;
      data.probability = (data.probability) ? data.probability : null;
      data.otherAmount = (data.otherAmount) ? data.otherAmount : 0;
      data.notes = (data.notes) ? data.notes : null;
      data.createDate = (data.createDate) ? new Date(data.createDate) : null;
      data.dollarAmount = (data.dollarAmount) ? data.dollarAmount : 0;
      data.contact_id = (data.contact_id) ? data.contact_id : null;
      data.company_id = (data.company_id) ? data.company_id : null;
      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}
