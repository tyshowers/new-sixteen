import { Image } from './image.model';
import { IState } from './i-state';
/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Blog
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      12/25/2017  Baselined
*****************************************************************************/

export class Blog implements IState {
  public $key: string;
  public id = '';
  public title: string;
  public headingFont: string;
  public headingFontSize: number;
  public carouselFont: string;
  public carouselFontSize: number;
  public bodyTextFont: string;
  public bodyTextFontSize: number;
  public carouselTextColor: string;
  public carouselTextBackgroundColor: string;
  public carouselAnimateIn: string;
  public carouselAnimateOut: string;
  public carouselDots: boolean = true;
  public carouselLoop: boolean = true;
  public carouselAutoplay: boolean = true;
  public carouselAutoplaySpeed: number;
  public carouselAutoplayHoverPause: boolean = true;
  public carouselDisplayItems: number = 1;
  public step;
  public description: string;
  public url: string;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = false;
  public deleted: boolean = false;
  public keywords:string;
  public user_id;
  public views: number;
  public lastViewed;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.title = (data.title) ? data.title : null;
      data.url = (data.url) ? data.url : null;
      data.description = (data.description) ? data.description : null;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.lastUpdated = (data.lastUpdated) ? new Date(data.lastUpdated) : null;
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.draft = (data.draft) ? data.draft : false;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}

export class Carousel implements IState {
  public id = '';
  public title: string;
  public step;
  public textFont: string;
  public headingTextColor: string;
  public textColor: string;
  public textBackgroundColor: string;
  public textPosition: string;
  public description: string;
  public url: string;
  public link: string;
  public images;
  public linkText: string;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = false;
  public deleted: boolean = false;
  public keywords:string;
  public user_id;
  public blog_id;
  public store_id;
  public views: number;
  public lastViewed;
  constructor() {

  }
}

export class Featurette implements IState {
  public id = '';
  public title: string;
  public step;
  public textFont: string;
  public textColor: string;
  public textBackgroundColor: string;
  public textPosition: string;
  public description: string;
  public url: string;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = false;
  public deleted: boolean = false;
  public keywords:string;
  public user_id;
  public blog_id;
  public store_id;
  public views: number;
  public lastViewed;
  constructor() {

  }
}

export class Parallax implements IState {
  public id = '';
  public title: string;
  public step;
  public textFont: string;
  public textColor: string;
  public textBackgroundColor: string;
  public textPosition: string;
  public description: string;
  public url: string;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = false;
  public deleted: boolean = false;
  public keywords:string;
  public user_id;
  public blog_id;
  public store_id;
  public views: number;
  public lastViewed;
  constructor() {

  }
}

export class ServiceBox implements IState {
  public id = '';
  public title: string;
  public step;
  public textFont: string;
  public textColor: string;
  public textBackgroundColor: string;
  public textPosition: string;
  public fadeShade: boolean = false;
  public description: string;
  public url: string;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = false;
  public deleted: boolean = false;
  public keywords:string;
  public user_id;
  public blog_id;
  public store_id;
  public views: number;
  public lastViewed;
  constructor() {

  }
}
export class Quote implements IState {
  public id = '';
  public name: string;
  public step;
  public textFont: string;
  public textColor: string;
  public textBackgroundColor: string;
  public textPosition: string;
  public description: string;
  public author: string;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = false;
  public deleted: boolean = false;
  public keywords:string;
  public user_id;
  public blog_id;
  public store_id;
  public views: number;
  public lastViewed;
  constructor() {

  }
}
