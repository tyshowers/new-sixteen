import { IState } from './i-state';
/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Setting
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      10/12/2017  Baselined
*****************************************************************************/
export class Receipt implements IState {
  public $key: string;
  public id;
  public payment_id;
  public order_id;
  public invoice_id;
  public shopping_cart_id;
  public store_id;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public user_id;
  public views: number;
  public lastViewed;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.store_id = (data.store_id) ? data.store_id : null;
      data.payment_id = (data.payment_id) ? data.payment_id : null;
      data.order_id = (data.order_id) ? data.order_id : null;
      data.invoice_id = (data.invoice_id) ? data.invoice_id : null;
      data.shopping_cart_id = (data.shopping_cart_id) ? data.shopping_cart_id : null;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.lastUpdated = (data.lastUpdated) ? new Date(data.lastUpdated) : null;
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.draft = (data.draft) ? data.draft : false;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}
