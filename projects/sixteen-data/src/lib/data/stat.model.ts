export class Stat {
  public $key: string;
  public id: string;
  public blog_id: string;
  public value: string;
  public topValue: string;
  public description: string;
  public lastUpdated;
  public lastUpdatedBy;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public user_id;
  public views: number;
  public lastViewed;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.blog_id = (data.blog_id) ? data.blog_id : null;
      data.value = (data.value) ? data.value : null;
      data.topValue = (data.topValue) ? data.topValue : null;
      data.description = (data.description) ? data.description : null;
      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}
