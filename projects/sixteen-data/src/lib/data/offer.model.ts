import { Image } from './image.model';
import { IDropdown } from './i-dropdown';
import { IState } from './i-state';
/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Offer
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      08/17/2017  Baselined
*  0.2      08/18/2017  Added state and dropdown interface
*****************************************************************************/

export class Offer implements IDropdown, IState {
  public $key: string;
  public id: string;
  public catalog_id;
  public product_id;
  public store_id;
  public name: string;
  public type: string;
  public title: string;
  public url: string;
  public description: string;
  public longDescription: string;
  public updated;
  public expirationDate;
  public iconURI: Image = null;
  public smallImageURI: Image = null;
  public largeImageURI: Image = null;
  public targetURI: string;
  public providerTag: string;
  public placement: string;
  public presentationMethod: string;
  public category: string;
  public attributes: string;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public user_id;
  public views: number;
  public lastViewed;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;



  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.store_id = (data.store_id) ? data.store_id : null;
      data.catalog_id = (data.catalog_id) ? data.catalog_id : null;
      data.product_id = (data.product_id) ? data.product_id : null;
      data.name = (data.name) ? data.name : null;
      data.url = (data.url) ? data.url : null;
      data.type = (data.type) ? data.type : null;
      data.title = (data.title) ? data.title : null;
      data.description = (data.description) ? data.description : null;
      data.longDescription = (data.longDescription) ? data.longDescription : null;
      data.updated = (data.updated) ? (new Date(data.updated))  : null;
      data.expirationDate = (data.expirationDate) ? (new Date(data.expirationDate)) : null;
      data.rating = (data.rating) ? data.rating : [];
      data.iconURI = (data.iconURI) ? data.iconURI : null;
      data.smallImageURI = (data.smallImageURI) ? data.smallImageURI : null;
      data.largeImageURI = (data.largeImageURI) ? data.largeImageURI : null;
      data.targetURI = (data.targetURI) ? data.targetURI : null;
      data.providerTag = (data.providerTag) ? data.providerTag : null;
      data.placement = (data.placement) ? data.placement : null;
      data.presentationMethod = (data.presentationMethod) ? data.presentationMethod : null;
      data.category = (data.category) ? data.category : null;
      data.attributes = (data.attributes) ? data.attributes : null;
      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}
