export class AppActivity {
  public $key: string;
  public id: string;
  public login?: Login;
  public logout?: Logout;
  public status?: string;
  public timeDifference;
  public lastUpdated;
  public lastUpdatedBy;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords:string;
  public user_id;
}

export class Login {
  public name: string;
  public start;
}
export class Logout {
  public name: string;
  public end;
}
