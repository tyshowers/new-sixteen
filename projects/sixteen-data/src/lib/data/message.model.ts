import { IState } from './i-state';

/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Message
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      11/13/2017  Baselined
*****************************************************************************/

export class Message implements IState {
  public $key: string;
  public id;
  public text = '';
  public name = '';
  public title;
  public photoURL;
  public imageUrl;
  public linkUrl;
  public article;
  public dataModel_id;
  public replies: Array<any> = [];
  public timeStamp = null;
  public favored = false;
  public bookmarked = false;
  public iconClass;
  public lastUpdated;
  public lastUpdatedBy;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public user_id;
  public views: number;
  public lastViewed;

  constructor() {

  }

  static restoreData(data: any) : void {
      data.id = (data.id) ? data.id : null;
      data.text = (data.text) ? data.text : null;
      data.name = (data.name) ? data.name : null;
      data.title = (data.title) ? data.title : null;
      data.photoURL = (data.photoURL) ? data.photoURL : null;
      data.linkUrl = (data.linkUrl) ? data.linkUrl : null;
      data.imageUrl = (data.imageUrl) ? data.imageUrl : null;
      data.replies = (data.replies) ? data.replies : [];
      data.timeStamp = (data.timeStamp) ? data.timeStamp : null;
      data.favored = (data.favored) ? data.favored : false;
      data.bookmarked = (data.bookmarked) ? data.bookmarked : false;
      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
  }
}
