import { IDropdown } from './i-dropdown';
import { IState } from './i-state';

/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Project
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      08/17/2017  Baselined
*  0.2      08/18/2017  Added state and dropdown interface
*  0.3      08/31/2017  Added addional classes that define a project
*  0.4      10/04/2017
*****************************************************************************/

export class Project implements IDropdown, IState {
  public $key: string;
  public id: string;
  public projectType: string;
  public status: string;
  public sections: Array<ReportSection> = [];
  public url: string;
  public startDate;
  public endDate;
  public billingEstimate: number;
  public description: string;
  public purchaseOrderNumber: string;
  public events: Array<any> = [];
  public milestones: Array<Milestone> = [];
  public requirements: Array<Requirement> = [];
  public affectedParties: Array<AffectedParty> = [];
  public implementationPlans: Array<ImplementationPlan> = [];
  public affectedSystems: Array<AffectedSystem> = [];
  public approvals: Array<Approval> = [];
  public sponsor: string;
  public sponsorType: string;
  public summaryOfBudget: string;
  public projectManager: string;
  public responsibilities: string;
  public purpose: string;
  public contact_id = null;
  public company_id = null;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public user_id;
  public keywords: string;
  public views: number;
  public lastViewed;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;
  public percentTimeComplete;
  public projectManagerName;

  constructor(public name: string) {

  }
  static restoreData(data: any): void {
    data.id = (data.id) ? data.id : null;
    data.name = (data.name) ? data.name : null;
    data.projectType = (data.projectType) ? data.projectType : null;
    data.status = (data.status) ? data.status : null;
    data.sections = (data.sections) ? data.sections : [];
    data.startDate = (data.startDate) ? new Date(data.startDate) : null;
    data.endDate = (data.endDate) ? new Date(data.endDate) : null;
    data.billingEstimate = (data.billingEstimate) ? data.billingEstimate : null;
    data.description = (data.description) ? data.description : null;
    data.purchaseOrderNumber = (data.purchaseOrderNumber) ? data.purchaseOrderNumber : null;
    data.events = (data.events) ? data.events : [];
    data.milestones = (data.milestones) ? data.milestones : [];
    data.requirements = (data.requirements) ? data.requirements : [];
    data.affectedParties = (data.affectedParties) ? data.affectedParties : [];
    data.implementationPlans = (data.implementationPlans) ? data.implementationPlans : [];
    data.affectedSystems = (data.affectedSystems) ? data.affectedSystems : [];
    data.approvals = (data.approvals) ? data.approvals : [];
    data.sponsor = (data.sponsor) ? data.sponsor : null;
    data.sponsorType = (data.sponsorType) ? data.sponsorType : null;
    data.summaryOfBudget = (data.summaryOfBudget) ? data.summaryOfBudget : null;
    data.projectManager = (data.projectManager) ? data.projectManager : null;
    data.responsibilities = (data.responsibilities) ? data.responsibilities : null;
    data.url = (data.url) ? data.url : null;
    data.purpose = (data.purpose) ? data.purpose : null;
    data.contact_id = (data.contact_id) ? data.contact_id : null;
    data.company_id = (data.company_id) ? data.company_id : null;
    data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
    data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
    data.draft = (data.draft) ? data.draft : false;
    data.deleted = (data.deleted) ? data.deleted : false;
    data.keywords = (data.keywords) ? data.keywords : null;
    data.user_id = (data.user_id) ? data.user_id : null;
    data.creatorName = (data.creatorName) ? data.creatorName : null;
    data.views = (data.views) ? data.views : 0;
    data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}

export class Milestone implements IState {
  public $key: string;
  public id;
  public date;
  public title: string;
  public description: string;
  public sections: Array<ReportSection> = [];
  public project_id;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = false;
  public deleted: boolean = false;
  public user_id;
  public keywords: string;
  public views: number;
  public lastViewed;
}

export class Requirement implements IState {
  public $key: string;
  public id;
  public project_id;
  public name;
  public description;
  public sections: Array<ReportSection> = [];
  public url;
  public requirementStatus;
  public user_id;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public views: number;
  public lastViewed;
  static restoreData(data: any): void {
    data.id = (data.id) ? data.id : null;
    data.name = (data.name) ? data.name : null;
    data.description = (data.description) ? data.description : null;
    data.sections = (data.sections) ? data.sections : [];
    data.url = (data.url) ? data.url : null;
    data.requirementStatus = (data.requirementStatus) ? data.requirementStatus : null;
    data.project_id = (data.project_id) ? data.project_id : null;
    data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
    data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
    data.draft = (data.draft) ? data.draft : false;
    data.deleted = (data.deleted) ? data.deleted : false;
    data.keywords = (data.keywords) ? data.keywords : null;
    data.user_id = (data.user_id) ? data.user_id : null;
    data.creatorName = (data.creatorName) ? data.creatorName : null;
    data.views = (data.views) ? data.views : 0;
    data.lastViewed = (data.lastViewed) ? data.lastViewed : (new Date());
  }
}

export class Deliverable implements IState {
  public $key: string;
  public id;
  public project_id;
  public name;
  public referenceURL;
  public deliverableStatus;
  public category;
  public sections: Array<ReportSection> = [];
  public description;
  public user_id;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public views: number;
  public lastViewed;
  static restoreData(data: any): void {
    data.id = (data.id) ? data.id : null;
    data.project_id = (data.project_id) ? data.project_id : null;
    data.name = (data.name) ? data.name : null;
    data.sections = (data.sections) ? data.sections : [];
    data.referenceURL = (data.referenceURL) ? data.referenceURL : null;
    data.deliverableStatus = (data.deliverableStatus) ? data.deliverableStatus : null;
    data.description = (data.description) ? data.description : null;
    data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
    data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
    data.draft = (data.draft) ? data.draft : false;
    data.deleted = (data.deleted) ? data.deleted : false;
    data.keywords = (data.keywords) ? data.keywords : null;
    data.user_id = (data.user_id) ? data.user_id : null;
    data.creatorName = (data.creatorName) ? data.creatorName : null;
    data.views = (data.views) ? data.views : 0;
    data.lastViewed = (data.lastViewed) ? data.lastViewed : (new Date());
  }
}

export class AffectedParty implements IState {
  public $key: string;
  public id;
  public project_id;
  public name;
  public description;
  public sections: Array<ReportSection> = [];
  public affectedPartyType;
  public userImage;
  public userName;
  public user_id;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public views: number;
  public lastViewed;
  static restoreData(data: any): void {
    data.id = (data.id) ? data.id : null;
    data.project_id = (data.project_id) ? data.project_id : null;
    data.name = (data.name) ? data.name : null;
    data.sections = (data.sections) ? data.sections : [];
    data.description = (data.description) ? data.description : null;
    data.affectedPartyType = (data.affectedPartyType) ? data.affectedPartyType : null;
    data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
    data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
    data.draft = (data.draft) ? data.draft : false;
    data.deleted = (data.deleted) ? data.deleted : false;
    data.keywords = (data.keywords) ? data.keywords : null;
    data.user_id = (data.user_id) ? data.user_id : null;
    data.creatorName = (data.creatorName) ? data.creatorName : null;
    data.views = (data.views) ? data.views : 0;
    data.lastViewed = (data.lastViewed) ? data.lastViewed : (new Date());
  }
}

export class AffectedSystem implements IState {
  public $key: string;
  id;
  project_id;
  name;
  description;
  public sections: Array<ReportSection> = [];
  public user_id;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public views: number;
  public lastViewed;
  static restoreData(data: any): void {
    data.id = (data.id) ? data.id : null;
    data.name = (data.name) ? data.name : null;
    data.sections = (data.sections) ? data.sections : [];
    data.project_id = (data.project_id) ? data.project_id : null;
    data.description = (data.description) ? data.description : null;
    data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
    data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
    data.draft = (data.draft) ? data.draft : false;
    data.deleted = (data.deleted) ? data.deleted : false;
    data.keywords = (data.keywords) ? data.keywords : null;
    data.user_id = (data.user_id) ? data.user_id : null;
    data.creatorName = (data.creatorName) ? data.creatorName : null;
    data.views = (data.views) ? data.views : 0;
    data.lastViewed = (data.lastViewed) ? data.lastViewed : (new Date());
  }
}

export class ImplementationPlan implements IState {
  public $key: string;
  public id;
  public project_id;
  public name;
  public description;
  public sections: Array<ReportSection> = [];
  public user_id;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public views: number;
  public lastViewed;
  static restoreData(data: any): void {
    data.id = (data.id) ? data.id : null;
    data.project_id = (data.project_id) ? data.project_id : null;
    data.name = (data.name) ? data.name : null;
    data.sections = (data.sections) ? data.sections : [];
    data.description = (data.description) ? data.description : null;
    data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
    data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
    data.draft = (data.draft) ? data.draft : false;
    data.deleted = (data.deleted) ? data.deleted : false;
    data.keywords = (data.keywords) ? data.keywords : null;
    data.user_id = (data.user_id) ? data.user_id : null;
    data.creatorName = (data.creatorName) ? data.creatorName : null;
    data.views = (data.views) ? data.views : 0;
    data.lastViewed = (data.lastViewed) ? data.lastViewed : (new Date());
  }
}
export class Bug implements IState {
  public $key: string;
  public id;
  public project_id;
  public deliverable_id;
  public name;
  public description;
  public currentState;
  public proposedSolution;
  public url;
  public bugStatus;
  public sections: Array<ReportSection> = [];
  public user_id;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public views: number;
  public lastViewed;
  static restoreData(data: any): void {
    data.id = (data.id) ? data.id : null;
    data.name = (data.name) ? data.name : null;
    data.deliverable_id = (data.deliverable_id) ? data.deliverable_id : null;
    data.description = (data.description) ? data.description : null;
    data.currentState = (data.currentState) ? data.currentState : null;
    data.proposedSolution = (data.proposedSolution) ? data.proposedSolution : null;
    data.sections = (data.sections) ? data.sections : [];
    data.url = (data.url) ? data.url : null;
    data.bugStatus = (data.bugStatus) ? data.bugStatus : null;
    data.project_id = (data.project_id) ? data.project_id : null;
    data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
    data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
    data.draft = (data.draft) ? data.draft : false;
    data.deleted = (data.deleted) ? data.deleted : false;
    data.keywords = (data.keywords) ? data.keywords : null;
    data.user_id = (data.user_id) ? data.user_id : null;
    data.creatorName = (data.creatorName) ? data.creatorName : null;
    data.views = (data.views) ? data.views : 0;
    data.lastViewed = (data.lastViewed) ? data.lastViewed : (new Date());
  }
}

export class ReportSection {
  public name;
  public description;
}

export class ProjectReport implements IState {
  public $key: string;
  public id;
  public project_id;
  public report_version: number = 0;
  public status;
  public name;
  public sections: Array<ReportSection> = [];
  public description;
  public user_id;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public views: number;
  public lastViewed;
  static restoreData(data: any): void {
    data.id = (data.id) ? data.id : null;
    data.name = (data.name) ? data.name : null;
    data.sections = (data.sections) ? data.sections : [];
    data.deliverables = (data.deliverables) ? data.deliverables : null;
    data.status = (data.status) ? data.status : null;
    data.report_version = (data.report_version) ? data.report_version : null;
    data.description = (data.description) ? data.description : null;
    data.project_id = (data.project_id) ? data.project_id : null;
    data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
    data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
    data.draft = (data.draft) ? data.draft : false;
    data.deleted = (data.deleted) ? data.deleted : false;
    data.keywords = (data.keywords) ? data.keywords : null;
    data.user_id = (data.user_id) ? data.user_id : null;
    data.creatorName = (data.creatorName) ? data.creatorName : null;
    data.views = (data.views) ? data.views : 0;
    data.lastViewed = (data.lastViewed) ? data.lastViewed : (new Date());
  }
}

export class Approval implements IState {
  public $key: string;
  public id;
  public project_id;
  public user_id;
  public userImage;
  public userName;
  public approvalDate;
  public approver;
  public approverName;
  public lastUpdatedBy;
  public lastUpdated;
  public sections: Array<ReportSection> = [];
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public views: number;
  public lastViewed;
  static restoreData(data: any): void {
    data.id = (data.id) ? data.id : null;
    data.project_id = (data.project_id) ? data.project_id : null;
    data.sections = (data.sections) ? data.sections : [];
    data.approver = (data.approver) ? data.approver : null;
    data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
    data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
    data.draft = (data.draft) ? data.draft : false;
    data.deleted = (data.deleted) ? data.deleted : false;
    data.keywords = (data.keywords) ? data.keywords : null;
    data.user_id = (data.user_id) ? data.user_id : null;
    data.creatorName = (data.creatorName) ? data.creatorName : null;
    data.views = (data.views) ? data.views : 0;
    data.lastViewed = (data.lastViewed) ? data.lastViewed : (new Date());
  }
}
