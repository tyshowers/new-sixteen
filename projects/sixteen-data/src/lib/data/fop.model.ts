import { IDropdown } from './i-dropdown';
import { IState } from './i-state';
/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Form of Payment
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      08/17/2017  Baselined
*  0.2      08/18/2017  Added state and dropdown interface
*****************************************************************************/
export class FOP implements IDropdown, IState  {
  public $key: string;
  public id : string;
  public name: string;
  public fopType : string;
  public ccType : string;
  public ccNumber : string;
  public expDate : string;
  public ccv : string;
  public bankName : string;
  public bankAccountNumber : string;
  public bankRoutingNumber : string;
  public checkNumber : string;
  public bankCity : string;
  public bankState : string;
  public bankPhone : string;
  public contact_id : string;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public user_id;
  public views: number;
  public lastViewed;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;

  static restoreData(data: any): void {
      data.name = (data.name) ? data.name : null;
      data.fopType = (data.fopType) ? data.fopType : null;
      data.ccType = (data.ccType) ? data.ccType : null;
      data.ccNumber = (data.ccNumber) ? data.ccNumber : null;
      data.expDate = (data.expDate) ? data.expDate : null;
      data.ccv = (data.ccv) ? data.ccv : null;
      data.bankName = (data.bankName) ? data.bankName : null;
      data.bankAccountNumber = (data.bankAccountNumber) ? data.bankAccountNumber : null;
      data.bankRoutingNumber = (data.bankRoutingNumber) ? data.bankRoutingNumber : null;
      data.checkNumber = (data.checkNumber) ? data.checkNumber : null;
      data.bankCity = (data.bankCity) ? data.bankCity : null;
      data.bankState = (data.bankState) ? data.bankState : null;
      data.bankPhone = (data.bankPhone) ? data.bankPhone : null;
      data.contact_id = (data.contact_id) ? data.contact_id : null;
      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}
