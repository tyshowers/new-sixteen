/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: User Format
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      11/11/2017  Baselined
*
*****************************************************************************/
export class User {
  public $key: string;
  public id?: string;
  public customer_id: string;
  public status: string;
  public emailProvider: string;
  public email: string;
  public name: string;
  public helpNeeded : boolean =true;
  public openView : boolean = false;
  public newsSources = [];
  public contact_id: string;
  public address_id: string;
  public phone_number_id: string;
  public fop_id: string;
  public email_address_id: string;
  public image: string;
  public shopping_cart_id: string;
  public messagesLastCheckedDate;
  public tasksLastCheckedDate;
  public alertsLastCheckedDate;
  public lastUpdated;
  public lastUpdatedBy;
  public creatorName;
  public createDate;
  public draft: boolean = true;
  public deleted: boolean = false;
  public introContactImport: boolean = false;
  public introCalendarImport: boolean = false;
  public welcomeMessageCount: number = 0;
  public keywords: string;
  public user_id;
  public workflow: Array<any>;
  public appActions: Array<any>;
  public groups: Array<any>;
  public currentStep;
  public currentStepName;
  public roles: Roles;
  public role;
  public profession;
  public gender;
  public firstName;
  public lastName;
  public moduleAccess: ModuleAccess;
  public referral;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.status = (data.status) ? data.status : "offline";
      data.name = (data.name) ? data.name : null;
      data.customer_id = (data.customer_id) ? data.customer_id : null;
      data.email = (data.email) ? data.email : null;
      data.helpNeeded = (data.helpNeeded) ? data.helpNeeded : true;
      data.openView = (data.openView) ? data.openView : false;
      data.newsSources = (data.newsSources) ? data.newsSources : [];
      data.emailProvider = (data.emailProvider) ? data.emailProvider : null;
      data.image = (data.image) ? (data.image) : null;
      data.messagesLastCheckedDate = (data.messagesLastCheckedDate) ? (data.messagesLastCheckedDate) : null;
      data.tasksLastCheckedDate = (data.tasksLastCheckedDate) ? (data.tasksLastCheckedDate) : null;
      data.alertsLastCheckedDate = (data.alertsLastCheckedDate) ? (data.alertsLastCheckedDate) : null;
      data.contact_id = (data.contact_id) ? data.contact_id : null;
      data.address_id = (data.address_id) ? data.address_id : null;
      data.phone_number_id = (data.phone_number_id) ? data.phone_number_id : null;
      data.fop_id = (data.fop_id) ? data.fop_id : null;
      data.email_address_id = (data.email_address_id) ? data.email_address_id : null;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.lastUpdated = (data.lastUpdated) ? new Date(data.lastUpdated) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.draft = (data.draft) ? data.draft : false;
      data.workflow = (data.workflow) ? data.workflow : null;
      data.appActions = (data.appActions) ? data.appActions : null;
      data.introContactImport = (data.introContactImport) ? data.introContactImport : false;
      data.introCalendarImport = (data.introCalendarImport) ? data.introCalendarImport : false;
      data.welcomeMessageCount = (data.welcomeMessageCount) ? data.welcomeMessageCount : 0;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.createDate = (data.createDate) ? data.createDate : null;
      data.roles = (data.roles) ? data.roles : {reader : true};
      data.moduleAccess = (data.moduleAccess) ? data.moduleAccess : {task : true};
      data.referral = (data.referral) ? data.referral : null;
  }
}

export interface Roles {
  reader: boolean;
  author?: boolean;
  admin?: boolean;
}

export interface ModuleAccess {
  help?: boolean;
  tasks?: boolean;
  news?: boolean;
  schedules?: boolean;
  messages?: boolean;
  opportunities?: boolean;
  projects?: boolean;
  stores?: boolean;
  contacts?: boolean;
  documents?: boolean;
  topics?: boolean;
  alerts?: boolean;
  goals?: boolean;
  blog?: boolean;
  timesheets?: boolean;
  emails?: boolean;
  settings?: boolean;
  semantics?: boolean;
  properties?: boolean;
}


export class Favorite {
  public $key: string;
  public id?: string;
  public name: string;
  public url: string;
  public link: string;
  public dataModel_id;
  public lastUpdated;
  public lastUpdatedBy;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public user_id;

}

export class Bookmark {
  public $key: string;
  public id?: string;
  public name: string;
  public url: string;
  public link: string;
  public dataModel_id;
  public lastUpdated;
  public lastUpdatedBy;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public user_id;

}
