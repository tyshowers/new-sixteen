import { IState } from './i-state';
import { TaTime } from './ta-date.model';

export class Task implements IState {
  public $key: string;
  public id = '';
  public name: string;
  public dueDate;
  public type;
  public even: boolean = false;
  public assignedToName = '';
  public isPastDate: boolean = false;
  public project_id: string;
  public description: string;
  public status: string;
  public timeToComplete : TaTime;
  public timerEndTime;
  public timerStartTime;
  public started: boolean = false;
  public assigned_to_id: string;
  public url: string;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = false;
  public deleted: boolean = false;
  public keywords: string;
  public user_id;
  public views: number;
  public lastViewed;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;
  public taskDate;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.name = (data.name) ? data.name : null;
      data.type = (data.type) ? data.type : null;
      data.dueDate = (data.dueDate) ? new Date(data.dueDate) : null;
      data.url = (data.url) ? data.url : null;
      data.description = (data.description) ? data.description : null;
      data.status = (data.status) ? data.status : null;
      data.timeToComplete = (data.timeToComplete) ? data.timeToComplete : null;
      data.timerEndTime = (data.timerEndTime) ? data.timerEndTime : null;
      data.project_id = (data.project_id) ? data.project_id : null;
      data.assigned_to_id = (data.assigned_to_id) ? data.assigned_to_id : null;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.lastUpdated = (data.lastUpdated) ? new Date(data.lastUpdated) : null;
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.draft = (data.draft) ? data.draft : false;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}
