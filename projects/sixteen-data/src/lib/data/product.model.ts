import { Image } from './image.model';
import { IDropdown } from './i-dropdown';
import { IState } from './i-state'
/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Product
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      08/17/2017  Baselined
*  0.2      08/18/2017  Added state and dropdown interface
*****************************************************************************/

export class Product implements IState, IDropdown {
  public $key: string;
  public id : string;
  public catalog_id;
  public store_id;
  public discontinued: boolean = false;
  public features;
  public image: Image;
  public url: string;
  public deliverable_id: string;
  public smallImage: Image;
  public alternateImage: Image;
  public description : string;
  public longDescription : string;
  public manufacturer : string;
  public author : string;
  public category : string;
  public leadTime: number = 0;
  public onSale: boolean = false;
  public salePrice: number = 0;
  public orderQuantity: number = 0;
  public orderQuantityMeasure: string;
  public price: number = 0;
  public hourlyRate: boolean = false;
  public reOrderLevel: number = 0;
  public sku : string;
  public subscription: boolean = false;
  public weight: number = 0;
  public height: number = 0;
  public width: number = 0;
  public length: number = 0;
  public unitsOnOrder: number = 0;
  public unitsInStock: number = 0;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public user_id;
  public views: number;
  public lastViewed;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;


  constructor(public name: string) {

  }
  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.store_id = (data.store_id) ? data.store_id : null;
      data.name = (data.name) ? data.name : null;
      data.catalog_id = (data.catalog_id) ? data.catalog_id : null;
      data.deliverable_id = (data.deliverable_id) ? data.deliverable_id : null;
      data.discontinued = (data.discontinued) ? data.discontinued : false;
      data.url = (data.url) ? data.url : null;
      data.image = (data.image) ? data.image : null;
      data.smallImage = (data.smallImage) ? data.smallImage : null;
      data.alternateImage = (data.alternateImage) ? data.alternateImage : null;
      data.description = (data.description) ? data.description : null;
      data.longDescription = (data.longDescription) ? data.longDescription : null;
      data.manufacturer = (data.manufacturer) ? data.manufacturer : null;
      data.author = (data.author) ? data.author : null;
      data.category = (data.category) ? data.category : null;
      data.leadTime = (data.leadTime) ? data.leadTime : 0;
      data.onSale = (data.onSale) ? data.onSale : false;
      data.salePrice = (data.salePrice) ? data.salePrice : 0;
      data.orderQuantity = (data.orderQuantity) ? data.orderQuantity : 0;
      data.price = (data.price) ? data.price : 0;
      data.hourlyRate = (data.hourlyRate) ? data.hourlyRate : false;
      data.reOrderLevel = (data.reOrderLevel) ? data.reOrderLevel : 0;
      data.sku = (data.sku) ? data.sku : null;
      data.subscription = (data.subscription) ? data.subscription : null;
      data.weight = (data.weight) ? data.weight : 0;
      data.height = (data.height) ? data.height : 0;
      data.width = (data.width) ? data.width : 0;
      data.length = (data.length) ? data.length : 0;
      data.unitsOnOrder = (data.unitsOnOrder) ? data.unitsOnOrder : 0;
      data.unitsInStock = (data.unitsInStock) ? data.unitsInStock : 0;
      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}
