import { IState } from './i-state';
/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Event
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      08/17/2017  Baselined
*  0.2      08/18/2017  Added state interface
*****************************************************************************/

export class Event implements IState {
  public $key: string;
  public id: string;
  public schedule_id: string;
  public project_id: string;
  public title: string;
  public eventType: string;
  public startDate;
  public endDate;
  public color;
  public actions;
  public allDay: boolean;
  public cssClass?: string;
  public resizable;
  public draggable?: boolean;
  public meta;
  public startTime: string;
  public endTime: string;
  public contacts: Array<any> = [];
  public attendees: Array<any> = [];
  public status : string;
  public location : string;
  public requiredStaffing : number = 0;
  public confirmed: boolean = false;
  public availableSpaces: number = 0;
  public costPerPerson: number = 0;
  public description : string;
  public recurrence: Recurrence;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public user_id;
  public views: number;
  public lastViewed;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.schedule_id = (data.schedule_id) ? data.schedule_id : null;
      data.title = (data.title) ? data.title : null;
      data.startDate = (data.startDate) ? data.startDate : null;
      data.endDate = (data.endDate) ? data.endDate : null;
      data.startTime = (data.startTime) ? data.startTime : null;
      data.endTime = (data.endTime) ? data.endTime : null;
      data.contacts = (data.contacts) ? data.contacts : [];
      data.attendees = (data.attendees) ? data.attendees : [];
      data.status = (data.status) ? data.status : null;
      data.location = (data.location) ? data.location : null;
      data.requiredStaffing = (data.requiredStaffing) ? data.requiredStaffing : 0;
      data.confirmed = (data.confirmed) ? data.confirmed : false;
      data.costPerPerson = (data.costPerPerson) ? data.costPerPerson : 0;
      data.availableSpaces = (data.availableSpaces) ? data.availableSpaces : 0;
      data.description = (data.description) ? data.description : null;
      data.recurrence = (data.recurrence) ? data.recurrence : null;
      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}

export class Recurrence {

  public id = '';
  public every: number = 0;
  public sunday: boolean = false;
  public monday: boolean = false;
  public tuesday: boolean = false;
  public wednesday: boolean = false;
  public thursday: boolean = false;
  public friday: boolean = false;
  public saturday: boolean = false;
  public dayOfMonth: '';
  public dayOfWeek: '';
  constructor(public startDate: Date, public endDate: Date) {

  }

}
