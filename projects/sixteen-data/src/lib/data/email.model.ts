import { EmailAddress } from './email-address.model';

import  { IState } from './i-state';
/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Email
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      08/17/2017  Baselined
*  0.2      08/18/2017  Added state interface
*****************************************************************************/

export class Email implements IState {
  public $key: string;
  public toAddresses: Array<any> = [];
  public fromAddress: EmailAddress;
  public fromFriendlyName: string = '';
  public createDate: string = null;
  public sentDate: string = null;
  public messageType = '';
  public attachments: Array<any> = [];
  public answered: boolean = false;
  public blindCopyAddresses: Array<any> = [];
  public copyAddresses: Array<any> = [];
  public multipart: boolean = false;
  public seen: boolean = false;
  public size: number = 0;
  public spam: boolean = false;
  public replyEmail: Email;
  public forwardEmail: Email;
  public emailFlag = '';
  public id = '';
  public user_id;
  public contact_id;
  public lastUpdated;
  public lastUpdatedBy;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public views: number;
  public lastViewed;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;

  constructor(public subject: string, public message: string) {}

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.subject = (data.subject) ? data.subject : null;
      data.message = (data.message) ? data.message : null;
      data.toAddresses = (data.toAddresses) ? data.toAddresses : [];
      data.fromAddress = (data.from.emailAddress.address) ? data.from.emailAddress.address : null;
      data.fromFriendlyName = (data.from.emailAddress.name) ? data.from.emailAddress.name : null;
      data.createDate = (data.receivedDateTime) ? new Date(data.receivedDateTime) : null;
      data.sentDate = (data.sentDate) ? new Date(data.sentDate) : null;
      data.messageType = (data.messageType) ? data.messageType : null;
      data.attachments = (data.attachments) ? data.attachments : [];
      data.answered = (data.answered) ? data.answered : [];
      data.blindCopyAddresses = (data.blindCopyAddresses) ? data.blindCopyAddresses : [];
      data.copyAddresses = (data.copyAddresses) ? data.copyAddresses : [];
      data.multipart = (data.multipart) ? data.multipart : false;
      data.seen = (data.isRead) ? data.isRead : false;
      data.size = (data.size) ? data.size : 0;
      data.spam = (data.spam) ? data.spam : false;
      data.replyEmail = (data.replyEmail) ? data.replyEmail : null;
      data.forwardEmail = (data.forwardEmail) ? data.forwardEmail : null;
      data.emailFlag = (data.emailFlag) ? data.emailFlag : 'received';
      data.user_id = (data.user_id) ? data.user_id : null;
      data.contact_id = (data.contact_id) ? data.contact_id : null;
      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}
