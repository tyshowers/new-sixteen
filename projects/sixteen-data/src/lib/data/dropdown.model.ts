import { IDropdown } from './i-dropdown';

/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Dropdown
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      08/17/2017  Baselined
*  0.2      08/18/2017  Added dropdown interface
*****************************************************************************/
export class Dropdown implements IDropdown {

  public index;

  constructor(public id: string, public name: string) {}

  static restoreData(data: any) {
    data.id = (data.id) ? data.id : null;
    data.name = (data.name) ? data.name : null;
  }
}

export class Dropdowns {
  professions = [];
  addressTypes = [];
  countries = [];
  creditCardTypes = [];
}
