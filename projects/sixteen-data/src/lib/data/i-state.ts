/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Data State Interface
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      08/17/2017  Baselined
*  0.2      09/17/2017  Added user ID/ person who created the record
*****************************************************************************/
export interface IState {
   user_id;
   lastUpdated;
   lastUpdatedBy;
   creatorName;
   deleted: boolean;
   draft: boolean;
   views: number;
   lastViewed;
   keywords;

}
