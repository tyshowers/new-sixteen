export class News {
  public status;
  public totalResults;
  public articles: NewsArticle[] = [];
}

export class NewsArticle {
  public id?: string;
  public source: Source;
  public author;
  public title;
  public description;
  public url;
  public urlToImage;
  public publishedAt;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;
}

export class Source {
  public id;
  public name;
}
