import { IState } from './i-state';
/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Image
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      08/17/2017  Baselined
*
*****************************************************************************/
export class Image {
  public $key: string;
  public id : string;
  public title : string;
  public alt : string;
  public src : string = '';
  public contact_id;
  public company_id;
  public product_id;
  public catalog_id;
  public store_id;
  public article_id;
  public carousel_id;
  public featurette_id;
  public service_box_id;
  public help_id;
  public lastUpdated;
  public lastUpdatedBy;
  public creatorName;
  public deleted: boolean;
  public draft: boolean;
  public keywords: string;
  public user_id;
  public views: number;
  public lastViewed;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.title = (data.title) ? data.title : null;
      data.alt = (data.alt) ? data.alt : null;
      data.src = (data.src) ? data.src : null;
      data.contact_id = (data.contact_id) ? data.contact_id : null;
      data.company_id = (data.company_id) ? data.company_id : null;
      data.product_id = (data.product_id) ? data.product_id : null;
      data.catalog_id = (data.catalog_id) ? data.catalog_id : null;
      data.store_id = (data.store_id) ? data.store_id : null;
      data.article_id = (data.article_id) ? data.article_id : null;
      data.carousel_id = (data.carousel_id) ? data.carousel_id : null;
      data.featurette_id = (data.featurette_id) ? data.featurette_id : null;
      data.service_box_id = (data.service_box_id) ? data.service_box_id : null;
      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
  }
}
