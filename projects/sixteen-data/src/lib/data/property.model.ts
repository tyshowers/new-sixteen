import { IState } from './i-state';
import {Address} from './address.model';

export class Property implements IState {
  public $key: string;
  public id = '';
  public name: string;
  public description: string;
  public url: string;
  public upload = [];
  public square_feet: number;
  public address: Address;
  public broker;
  public agent_id;
  public agent_name;
  public pets: boolean = false;
  public bedrooms: number;
  public bathrooms: number;
  public garages: number;
  public price: number;
  public date_listed;
  public date_sold;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords:string;
  public user_id;
  public views: number;
  public viewsByDay = [];
  public lastViewed;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.name = (data.name) ? data.name : null;
      data.url = (data.url) ? data.url : null;
      data.description = (data.description) ? data.description : null;
      data.square_feet = (data.square_feet) ? data.square_feet : 0;
      data.address = (data.address) ? data.address : null;
      data.broker = (data.broker) ? data.broker : null;
      data.agent_id = (data.agent_id) ? data.agent_id : null;
      data.agent_name = (data.agent_name) ? data.agent_name : null;
      data.pets = (data.pets) ? data.pets : false;
      data.bedrooms = (data.bedrooms) ? data.bedrooms : 0;
      data.bathrooms = (data.bathrooms) ? data.bathrooms : 0;
      data.garages = (data.garages) ? data.garages : 0;
      data.price = (data.price) ? data.price : 0;
      data.date_listed = (data.date_listed) ? data.date_listed : null;
      data.date_sold = (data.date_sold) ? data.date_sold : null;
      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.views = (data.views) ? data.views : 0;
      data.viewsByDay = (data.viewsByDay) ? data.viewsByDay : [];
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
      data.bookmarked = (data.bookmarked) ? data.bookmarked : null;
      data.bookmarkedCount = (data.bookmarkedCount) ? data.bookmarkedCount : null;
      data.favored = (data.favored) ? data.favored :null;
      data.favoredCount = (data.favoredCount) ? data.favoredCount : null;
      data.broadcasted = (data.broadcasted) ? data.broadcasted : null;
      data.broadcastedCount = (data.broadcastedCount) ? data.broadcastedCount :null;
  }
}

export class Showing implements IState {
  public $key: string;
  public id = '';
  public show_date;
  public description: string;
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords:string;
  public user_id;
  public views: number;
  public lastViewed;

}
