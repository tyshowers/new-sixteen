import { TaDate } from './ta-date.model';
import { IState } from './i-state';
import { IorderLine } from './shopping-cart.model'
/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Order
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      08/17/2017  Baselined
*  0.2      08/18/2017  Added state interface
*****************************************************************************/

export class Order implements IState {
  public $key: string;
  public id : string;
  public catalog_id;
  public company_id;
  public contact_id;
  public store_id;
  public shopping_cart_id;
  public address_id;
  public fop_id;
  public shippingRequired: boolean = false;
  public orderDate;
  public date: TaDate = null;
  public amount: number = 0;
  public tax: number = 0;
  public status : string;
  public orderLine : Array<IorderLine> = [];
  public invoices : Array<any> = [];
  public lastUpdatedBy;
  public lastUpdated;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords: string;
  public user_id;
  public views: number;
  public lastViewed;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;

  static restoreData(data: any): void {
      data.id = (data.id) ? data.id : null;
      data.store_id = (data.store_id) ? data.store_id : null;
      data.catalog_id = (data.catalog_id) ? data.catalog_id : null;
      data.date = (data.date) ? new Date(data.date) : null;
      data.contact_id = (data.contact_id) ? data.contact_id : null;
      data.company_id = (data.company_id) ? data.company_id : null;
      data.shopping_cart_id = (data.shopping_cart_id) ? data.shopping_cart_id : null;
      data.address_id = (data.address_id) ? data.address_id : null;
      data.fop_id = (data.fop_id) ? data.fop_id : null;
      data.shippingRequired = (data.shippingRequired) ? data.shippingRequired : false;
      data.orderDate = (data.orderDate) ? data.orderDate : null;
      data.amount = (data.amount) ? data.amount : 0;
      data.tax = (data.tax) ? data.tax : 0;
      data.status = (data.status) ? data.status : null;
      data.orderLine = (data.orderLine) ? data.orderLine : [];
      data.invoices = (data.invoices) ? data.invoices : [];
      data.lastUpdated = (data.lastUpdated) ? (new Date(data.lastUpdated)) : (new Date());
      data.lastUpdatedBy = (data.lastUpdatedBy) ? data.lastUpdatedBy : null;
      data.draft = (data.draft) ? data.draft : false;
      data.deleted = (data.deleted) ? data.deleted : false;
      data.keywords = (data.keywords) ? data.keywords : null;
      data.user_id = (data.user_id) ? data.user_id : null;
      data.creatorName = (data.creatorName) ? data.creatorName : null;
      data.views = (data.views) ? data.views : 0;
      data.lastViewed = (data.lastViewed) ? new Date(data.lastViewed) : (new Date());
  }
}
