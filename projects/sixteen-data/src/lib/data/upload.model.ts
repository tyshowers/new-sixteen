/*****************************************************************************
*                 Taliferro License Notice
*
* The contents of this file are subject to the Taliferro License
* (the "License"). You may not use this file except in
* compliance with the License. A copy of the License is available at
* http://www.taliferro.com/license/
*
*
* Title: Upload File Format
* @author Tyrone Showers
*
* @copyright 1997-2018 Taliferro, Inc. All Rights Reserved.
*
*        Change Log
*
* Version     Date       Description
* -------   ----------  -------------------------------------------------------
*  0.1      11/11/2017  Baselined
*
*****************************************************************************/
export class Upload {
  public $key: string;
  public id: string;
  public file: File;
  public name: string;
  public originalName: string;
  public byteSize: number;
  public thumbnail: string;
  public ref;
  public url: string;
  public article_id;
  public group_id;
  public carousel_id;
  public featurette_id;
  public parallax_id;
  public service_box_id;
  public product_bundle_id;
  public contact_id;
  public project_id;
  public property_id;
  public order_id;
  public help_id;
  public event_id;
  public message_id;
  public product_id;
  public catalog_id;
  public topic_id;
  public store_id;
  public offer_id;
  public progress: number;
  public lastUpdated;
  public lastUpdatedBy;
  public creatorName;
  public draft: boolean = true;
  public deleted: boolean = false;
  public keywords;
  public user_id;
  public bookmarked;
  public bookmarkedCount;
  public favored;
  public favoredCount;
  public broadcasted;
  public broadcastedCount;

  constructor(file: File) {
    this.file = file;
  }

}
