import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerHowTo3Component } from './sixteen-customer-how-to3.component';

describe('SixteenCustomerHowTo3Component', () => {
  let component: SixteenCustomerHowTo3Component;
  let fixture: ComponentFixture<SixteenCustomerHowTo3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerHowTo3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerHowTo3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
