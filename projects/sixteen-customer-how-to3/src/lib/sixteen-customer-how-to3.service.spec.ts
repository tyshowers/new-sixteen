import { TestBed } from '@angular/core/testing';

import { SixteenCustomerHowTo3Service } from './sixteen-customer-how-to3.service';

describe('SixteenCustomerHowTo3Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerHowTo3Service = TestBed.get(SixteenCustomerHowTo3Service);
    expect(service).toBeTruthy();
  });
});
