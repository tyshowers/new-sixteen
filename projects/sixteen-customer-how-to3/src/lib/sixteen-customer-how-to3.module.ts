import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerHowTo3Component } from './sixteen-customer-how-to3.component';

@NgModule({
  declarations: [SixteenCustomerHowTo3Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerHowTo3Component]
})
export class SixteenCustomerHowTo3Module { }
