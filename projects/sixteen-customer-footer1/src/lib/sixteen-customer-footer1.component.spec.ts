import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerFooter1Component } from './sixteen-customer-footer1.component';

describe('SixteenCustomerFooter1Component', () => {
  let component: SixteenCustomerFooter1Component;
  let fixture: ComponentFixture<SixteenCustomerFooter1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerFooter1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerFooter1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
