import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerFooter1Component } from './sixteen-customer-footer1.component';

@NgModule({
  declarations: [SixteenCustomerFooter1Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerFooter1Component]
})
export class SixteenCustomerFooter1Module { }
