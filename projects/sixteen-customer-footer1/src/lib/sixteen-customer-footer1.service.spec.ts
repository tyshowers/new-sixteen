import { TestBed } from '@angular/core/testing';

import { SixteenCustomerFooter1Service } from './sixteen-customer-footer1.service';

describe('SixteenCustomerFooter1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerFooter1Service = TestBed.get(SixteenCustomerFooter1Service);
    expect(service).toBeTruthy();
  });
});
