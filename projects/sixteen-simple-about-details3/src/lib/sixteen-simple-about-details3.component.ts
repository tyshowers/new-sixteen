import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lib-sixteen-simple-about-details3',
  templateUrl: './sixteen-simple-about-details3.component.html',
  styles: []
})
export class SixteenSimpleAboutDetails3Component implements OnInit {

  @Input() headingText = "Full UI Customization";
  @Input() descriptionText = "Ducimus, qui dolorem ipsum, quia consequuntur magni. Autem vel illum, qui ratione voluptatem sequi. Eius modi tempora incidunt, ut enim ipsam voluptatem. Quas molestias excepturi sint, obcaecati cupiditate.";
  @Input() buttonLink;
  @Input() buttonText = "Read More";
  @Input() imageURL = "http://via.placeholder.com/1010x710";

  @Input() featureIcon = "icon-layers";
  @Input() featureText = "User-friendly Interface";

  constructor() { }

  ngOnInit() {
  }

}
