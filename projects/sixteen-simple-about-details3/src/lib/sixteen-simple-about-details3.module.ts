import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenSimpleAboutDetails3Component } from './sixteen-simple-about-details3.component';

@NgModule({
  declarations: [SixteenSimpleAboutDetails3Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenSimpleAboutDetails3Component]
})
export class SixteenSimpleAboutDetails3Module { }
