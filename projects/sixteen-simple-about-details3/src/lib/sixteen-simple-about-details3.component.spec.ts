import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenSimpleAboutDetails3Component } from './sixteen-simple-about-details3.component';

describe('SixteenSimpleAboutDetails3Component', () => {
  let component: SixteenSimpleAboutDetails3Component;
  let fixture: ComponentFixture<SixteenSimpleAboutDetails3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenSimpleAboutDetails3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenSimpleAboutDetails3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
