import { TestBed } from '@angular/core/testing';

import { SixteenSimpleAboutDetails3Service } from './sixteen-simple-about-details3.service';

describe('SixteenSimpleAboutDetails3Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenSimpleAboutDetails3Service = TestBed.get(SixteenSimpleAboutDetails3Service);
    expect(service).toBeTruthy();
  });
});
