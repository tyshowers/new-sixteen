import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerContactUs1Component } from './sixteen-customer-contact-us1.component';

describe('SixteenCustomerContactUs1Component', () => {
  let component: SixteenCustomerContactUs1Component;
  let fixture: ComponentFixture<SixteenCustomerContactUs1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerContactUs1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerContactUs1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
