import { TestBed } from '@angular/core/testing';

import { SixteenCustomerContactUs1Service } from './sixteen-customer-contact-us1.service';

describe('SixteenCustomerContactUs1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerContactUs1Service = TestBed.get(SixteenCustomerContactUs1Service);
    expect(service).toBeTruthy();
  });
});
