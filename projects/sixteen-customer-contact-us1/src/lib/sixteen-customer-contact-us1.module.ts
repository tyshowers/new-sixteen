import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SixteenCustomerContactUs1Component } from './sixteen-customer-contact-us1.component';

@NgModule({
  declarations: [SixteenCustomerContactUs1Component],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule
  ],
  exports: [SixteenCustomerContactUs1Component]
})
export class SixteenCustomerContactUs1Module { }
