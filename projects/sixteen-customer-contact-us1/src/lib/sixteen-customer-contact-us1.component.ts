import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lib-sixteen-customer-contact-us1',
  templateUrl: './sixteen-customer-contact-us1.component.html',
  styles: []
})
export class SixteenCustomerContactUs1Component implements OnInit {

  @Input() headingText = "Contact Us";
  @Input() descriptionText = "We don't like spam too";

  constructor() { }

  ngOnInit() {
  }

  onSubmit() {
    console.log("Button clicked")
  }

}
