import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenTaskNotificationComponent } from './sixteen-task-notification.component';

describe('SixteenTaskNotificationComponent', () => {
  let component: SixteenTaskNotificationComponent;
  let fixture: ComponentFixture<SixteenTaskNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenTaskNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenTaskNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
