import { TestBed } from '@angular/core/testing';

import { SixteenTaskNotificationService } from './sixteen-task-notification.service';

describe('SixteenTaskNotificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenTaskNotificationService = TestBed.get(SixteenTaskNotificationService);
    expect(service).toBeTruthy();
  });
});
