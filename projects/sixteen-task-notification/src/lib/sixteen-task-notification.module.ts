import { NgModule } from '@angular/core';
import { SixteenTaskNotificationComponent } from './sixteen-task-notification.component';

@NgModule({
  declarations: [SixteenTaskNotificationComponent],
  imports: [
  ],
  exports: [SixteenTaskNotificationComponent]
})
export class SixteenTaskNotificationModule { }
