import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewsPickerComponent } from './news-picker/news-picker.component';
import { NewsViewComponent } from './news-view/news-view.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: NewsViewComponent, data: { title: 'News', state: 'news list' } },
      { path: 'selections', component: NewsPickerComponent, data: { title: 'News Selection', state: 'news selection' } },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReaderRoutingModule { }
