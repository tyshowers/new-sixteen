import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { ReaderRoutingModule } from './reader-routing.module';
import { NewsPickerComponent } from './news-picker/news-picker.component';
import { NewsViewComponent } from './news-view/news-view.component';

@NgModule({
  declarations: [NewsPickerComponent, NewsViewComponent],
  imports: [
    CommonModule,
    ReaderRoutingModule,
    SharedModule
  ]
})
export class ReaderModule { }
