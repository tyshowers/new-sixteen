import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReaderModule } from './reader/reader.module';

const routes: Routes = [
  { path: 'news', children: [ {path: '', loadChildren: './reader/reader.module#ReaderModule'} ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
