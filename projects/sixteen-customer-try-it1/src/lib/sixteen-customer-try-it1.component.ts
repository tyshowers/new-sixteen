import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lib-sixteen-customer-try-it1',
  templateUrl: './sixteen-customer-try-it1.component.html',
  styles: []
})
export class SixteenCustomerTryIt1Component implements OnInit {

  @Input() headingText = "Try it free for 14 day";
  @Input() buttonLink;
  @Input() buttonText = "Start Free Trial";

  constructor() { }

  ngOnInit() {
  }

}
