import { TestBed } from '@angular/core/testing';

import { SixteenCustomerTryIt1Service } from './sixteen-customer-try-it1.service';

describe('SixteenCustomerTryIt1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerTryIt1Service = TestBed.get(SixteenCustomerTryIt1Service);
    expect(service).toBeTruthy();
  });
});
