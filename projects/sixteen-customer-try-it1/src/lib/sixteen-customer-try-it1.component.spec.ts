import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerTryIt1Component } from './sixteen-customer-try-it1.component';

describe('SixteenCustomerTryIt1Component', () => {
  let component: SixteenCustomerTryIt1Component;
  let fixture: ComponentFixture<SixteenCustomerTryIt1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerTryIt1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerTryIt1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
