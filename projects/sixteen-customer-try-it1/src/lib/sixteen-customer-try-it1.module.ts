import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerTryIt1Component } from './sixteen-customer-try-it1.component';

@NgModule({
  declarations: [SixteenCustomerTryIt1Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerTryIt1Component]
})
export class SixteenCustomerTryIt1Module { }
