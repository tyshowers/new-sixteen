import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerHeader1Component } from './sixteen-customer-header1.component';

describe('SixteenCustomerHeader1Component', () => {
  let component: SixteenCustomerHeader1Component;
  let fixture: ComponentFixture<SixteenCustomerHeader1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerHeader1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerHeader1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
