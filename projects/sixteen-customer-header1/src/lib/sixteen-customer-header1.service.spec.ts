import { TestBed } from '@angular/core/testing';

import { SixteenCustomerHeader1Service } from './sixteen-customer-header1.service';

describe('SixteenCustomerHeader1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerHeader1Service = TestBed.get(SixteenCustomerHeader1Service);
    expect(service).toBeTruthy();
  });
});
