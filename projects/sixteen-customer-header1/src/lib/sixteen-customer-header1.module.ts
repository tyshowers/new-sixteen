import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerHeader1Component } from './sixteen-customer-header1.component';

@NgModule({
  declarations: [SixteenCustomerHeader1Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerHeader1Component]
})
export class SixteenCustomerHeader1Module { }
