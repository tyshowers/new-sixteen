import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerPrices2Component } from './sixteen-customer-prices2.component';
import { SixteenDataModule } from 'sixteen-data';

@NgModule({
  declarations: [SixteenCustomerPrices2Component],
  imports: [
    CommonModule,
    RouterModule,
    SixteenDataModule
  ],
  exports: [SixteenCustomerPrices2Component]
})
export class SixteenCustomerPrices2Module { }
