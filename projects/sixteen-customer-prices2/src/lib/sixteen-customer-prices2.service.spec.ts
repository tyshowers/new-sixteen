import { TestBed } from '@angular/core/testing';

import { SixteenCustomerPrices2Service } from './sixteen-customer-prices2.service';

describe('SixteenCustomerPrices2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerPrices2Service = TestBed.get(SixteenCustomerPrices2Service);
    expect(service).toBeTruthy();
  });
});
