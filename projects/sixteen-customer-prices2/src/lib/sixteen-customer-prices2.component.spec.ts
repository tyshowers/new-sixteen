import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerPrices2Component } from './sixteen-customer-prices2.component';

describe('SixteenCustomerPrices2Component', () => {
  let component: SixteenCustomerPrices2Component;
  let fixture: ComponentFixture<SixteenCustomerPrices2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerPrices2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerPrices2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
