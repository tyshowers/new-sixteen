import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'sixteen-data';

@Component({
  selector: 'lib-sixteen-customer-prices2',
  templateUrl: './sixteen-customer-prices2.component.html',
  styles: []
})
export class SixteenCustomerPrices2Component implements OnInit {

  @Input() headingText = "Prices";
  @Input() descriptionText = "Corporis suscipit laboriosam, nisi ut perspiciatis, unde omnis dolor repellendus. Suscipit laboriosam, nisi ut enim ad minima veniam. Id, quod maxime placeat, facere possimus, omnis iste natus.";
  @Input() products : Product[] = [];
  @Input() actionText = "Get Started";

  constructor() { }

  ngOnInit() {
  }

}
