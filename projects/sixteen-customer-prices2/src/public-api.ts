/*
 * Public API Surface of sixteen-customer-prices2
 */

export * from './lib/sixteen-customer-prices2.service';
export * from './lib/sixteen-customer-prices2.component';
export * from './lib/sixteen-customer-prices2.module';
