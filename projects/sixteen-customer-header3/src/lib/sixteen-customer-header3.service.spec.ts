import { TestBed } from '@angular/core/testing';

import { SixteenCustomerHeader3Service } from './sixteen-customer-header3.service';

describe('SixteenCustomerHeader3Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerHeader3Service = TestBed.get(SixteenCustomerHeader3Service);
    expect(service).toBeTruthy();
  });
});
