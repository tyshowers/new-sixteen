import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerHeader3Component } from './sixteen-customer-header3.component';

describe('SixteenCustomerHeader3Component', () => {
  let component: SixteenCustomerHeader3Component;
  let fixture: ComponentFixture<SixteenCustomerHeader3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerHeader3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerHeader3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
