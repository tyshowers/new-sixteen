import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerHeader3Component } from './sixteen-customer-header3.component';

@NgModule({
  declarations: [SixteenCustomerHeader3Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerHeader3Component]
})
export class SixteenCustomerHeader3Module { }
