/*
 * Public API Surface of sixteen-customer-header3
 */

export * from './lib/sixteen-customer-header3.service';
export * from './lib/sixteen-customer-header3.component';
export * from './lib/sixteen-customer-header3.module';
