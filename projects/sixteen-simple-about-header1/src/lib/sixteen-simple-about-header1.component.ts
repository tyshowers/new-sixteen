import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lib-sixteen-simple-about-header1',
  templateUrl: './sixteen-simple-about-header1.component.html',
  styles: []
})
export class SixteenSimpleAboutHeader1Component implements OnInit {

  @Input() headingText = "About";
  @Input() imageURL = "";

  constructor() { }

  ngOnInit() {
  }

}
