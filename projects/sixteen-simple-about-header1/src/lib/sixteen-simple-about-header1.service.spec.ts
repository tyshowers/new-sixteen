import { TestBed } from '@angular/core/testing';

import { SixteenSimpleAboutHeader1Service } from './sixteen-simple-about-header1.service';

describe('SixteenSimpleAboutHeader1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenSimpleAboutHeader1Service = TestBed.get(SixteenSimpleAboutHeader1Service);
    expect(service).toBeTruthy();
  });
});
