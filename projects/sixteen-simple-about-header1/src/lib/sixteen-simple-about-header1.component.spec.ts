import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenSimpleAboutHeader1Component } from './sixteen-simple-about-header1.component';

describe('SixteenSimpleAboutHeader1Component', () => {
  let component: SixteenSimpleAboutHeader1Component;
  let fixture: ComponentFixture<SixteenSimpleAboutHeader1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenSimpleAboutHeader1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenSimpleAboutHeader1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
