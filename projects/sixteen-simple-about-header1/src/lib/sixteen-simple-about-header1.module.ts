import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenSimpleAboutHeader1Component } from './sixteen-simple-about-header1.component';

@NgModule({
  declarations: [SixteenSimpleAboutHeader1Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenSimpleAboutHeader1Component]
})
export class SixteenSimpleAboutHeader1Module { }
