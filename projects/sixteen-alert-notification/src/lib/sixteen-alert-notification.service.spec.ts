import { TestBed } from '@angular/core/testing';

import { SixteenAlertNotificationService } from './sixteen-alert-notification.service';

describe('SixteenAlertNotificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenAlertNotificationService = TestBed.get(SixteenAlertNotificationService);
    expect(service).toBeTruthy();
  });
});
