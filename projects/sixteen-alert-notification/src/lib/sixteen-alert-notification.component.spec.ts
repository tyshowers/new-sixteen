import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenAlertNotificationComponent } from './sixteen-alert-notification.component';

describe('SixteenAlertNotificationComponent', () => {
  let component: SixteenAlertNotificationComponent;
  let fixture: ComponentFixture<SixteenAlertNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenAlertNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenAlertNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
