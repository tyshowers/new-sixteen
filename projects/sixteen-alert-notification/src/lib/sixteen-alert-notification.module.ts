import { NgModule } from '@angular/core';
import { SixteenAlertNotificationComponent } from './sixteen-alert-notification.component';

@NgModule({
  declarations: [SixteenAlertNotificationComponent],
  imports: [
  ],
  exports: [SixteenAlertNotificationComponent]
})
export class SixteenAlertNotificationModule { }
