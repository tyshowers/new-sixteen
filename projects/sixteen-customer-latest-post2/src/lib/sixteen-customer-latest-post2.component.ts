import { Component, OnInit, Input } from '@angular/core';
import { Article } from 'sixteen-data';

@Component({
  selector: 'lib-sixteen-customer-latest-post2',
  templateUrl: './sixteen-customer-latest-post2.component.html',
  styles: []
})
export class SixteenCustomerLatestPost2Component implements OnInit {

  @Input() headingText = "Latest Posts";
  @Input() descriptionText = "Impedit, quo voluptas nulla vero. Quisquam est, omnis voluptas sit, amet, consectetur, adipisci velit, sed quia. Ratione voluptatem sequi nesciunt, neque porro quisquam est omnis.";
  @Input() articles : Article[] = [];

  constructor() { }

  ngOnInit() {
  }

}
