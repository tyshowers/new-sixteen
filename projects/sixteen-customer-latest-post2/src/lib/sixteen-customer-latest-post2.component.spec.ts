import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerLatestPost2Component } from './sixteen-customer-latest-post2.component';

describe('SixteenCustomerLatestPost2Component', () => {
  let component: SixteenCustomerLatestPost2Component;
  let fixture: ComponentFixture<SixteenCustomerLatestPost2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerLatestPost2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerLatestPost2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
