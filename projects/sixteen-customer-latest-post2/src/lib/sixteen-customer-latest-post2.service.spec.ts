import { TestBed } from '@angular/core/testing';

import { SixteenCustomerLatestPost2Service } from './sixteen-customer-latest-post2.service';

describe('SixteenCustomerLatestPost2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerLatestPost2Service = TestBed.get(SixteenCustomerLatestPost2Service);
    expect(service).toBeTruthy();
  });
});
