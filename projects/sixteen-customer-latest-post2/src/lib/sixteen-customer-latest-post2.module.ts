import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerLatestPost2Component } from './sixteen-customer-latest-post2.component';
import { SixteenDataModule } from 'sixteen-data';

@NgModule({
  declarations: [SixteenCustomerLatestPost2Component],
  imports: [
    CommonModule,
    RouterModule,
    SixteenDataModule
  ],
  exports: [SixteenCustomerLatestPost2Component]
})
export class SixteenCustomerLatestPost2Module { }
