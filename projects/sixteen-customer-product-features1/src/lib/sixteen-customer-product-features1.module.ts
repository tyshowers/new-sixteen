import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerProductFeatures1Component } from './sixteen-customer-product-features1.component';

@NgModule({
  declarations: [SixteenCustomerProductFeatures1Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerProductFeatures1Component]
})
export class SixteenCustomerProductFeatures1Module { }
