import { TestBed } from '@angular/core/testing';

import { SixteenCustomerProductFeatures1Service } from './sixteen-customer-product-features1.service';

describe('SixteenCustomerProductFeatures1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerProductFeatures1Service = TestBed.get(SixteenCustomerProductFeatures1Service);
    expect(service).toBeTruthy();
  });
});
