import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerProductFeatures1Component } from './sixteen-customer-product-features1.component';

describe('SixteenCustomerProductFeatures1Component', () => {
  let component: SixteenCustomerProductFeatures1Component;
  let fixture: ComponentFixture<SixteenCustomerProductFeatures1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerProductFeatures1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerProductFeatures1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
