/*
 * Public API Surface of sixteen-simple-about-details2
 */

export * from './lib/sixteen-simple-about-details2.service';
export * from './lib/sixteen-simple-about-details2.component';
export * from './lib/sixteen-simple-about-details2.module';
