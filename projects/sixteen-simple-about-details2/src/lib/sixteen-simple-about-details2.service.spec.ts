import { TestBed } from '@angular/core/testing';

import { SixteenSimpleAboutDetails2Service } from './sixteen-simple-about-details2.service';

describe('SixteenSimpleAboutDetails2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenSimpleAboutDetails2Service = TestBed.get(SixteenSimpleAboutDetails2Service);
    expect(service).toBeTruthy();
  });
});
