import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lib-sixteen-simple-about-details2',
  templateUrl: './sixteen-simple-about-details2.component.html',
  styles: []
})
export class SixteenSimpleAboutDetails2Component implements OnInit {

  @Input() headingText = "Unlimited Service Capabilities";
  @Input() descriptionText = "Quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut perspiciatis unde. Nemo enim ipsam voluptatem, quia voluptas nulla vero eos. Alias consequatur aut perferendis doloribus asperiores repellat.";
  @Input() buttonLink;
  @Input() buttonText = "Read More";

  @Input() featureIcon1 = "icon-layers";
  @Input() featureText1 = "User-friendly Interface";

  @Input() featureIcon2 = "icon-speedometer";
  @Input() featureText2 = "High Download Speed";

  @Input() featureIcon3 = "icon-equalizer";
  @Input() featureText3 = "Full UI Customization";

  @Input() featureIcon4 = "icon-cloud-upload";
  @Input() featureText4 = "Unlimited Cloud Storage";

  constructor() { }

  ngOnInit() {
  }

}
