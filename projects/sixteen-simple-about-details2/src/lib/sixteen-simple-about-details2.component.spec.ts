import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenSimpleAboutDetails2Component } from './sixteen-simple-about-details2.component';

describe('SixteenSimpleAboutDetails2Component', () => {
  let component: SixteenSimpleAboutDetails2Component;
  let fixture: ComponentFixture<SixteenSimpleAboutDetails2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenSimpleAboutDetails2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenSimpleAboutDetails2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
