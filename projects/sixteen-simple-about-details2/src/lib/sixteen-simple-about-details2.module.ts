import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenSimpleAboutDetails2Component } from './sixteen-simple-about-details2.component';

@NgModule({
  declarations: [SixteenSimpleAboutDetails2Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenSimpleAboutDetails2Component]
})
export class SixteenSimpleAboutDetails2Module { }
