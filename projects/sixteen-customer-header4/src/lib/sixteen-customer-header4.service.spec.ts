import { TestBed } from '@angular/core/testing';

import { SixteenCustomerHeader4Service } from './sixteen-customer-header4.service';

describe('SixteenCustomerHeader4Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerHeader4Service = TestBed.get(SixteenCustomerHeader4Service);
    expect(service).toBeTruthy();
  });
});
