import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SixteenCustomerHeader4Component } from './sixteen-customer-header4.component';

@NgModule({
  declarations: [SixteenCustomerHeader4Component],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule
  ],
  exports: [SixteenCustomerHeader4Component]
})
export class SixteenCustomerHeader4Module { }
