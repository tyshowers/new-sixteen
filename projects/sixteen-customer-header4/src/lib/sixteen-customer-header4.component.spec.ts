import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerHeader4Component } from './sixteen-customer-header4.component';

describe('SixteenCustomerHeader4Component', () => {
  let component: SixteenCustomerHeader4Component;
  let fixture: ComponentFixture<SixteenCustomerHeader4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerHeader4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerHeader4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
