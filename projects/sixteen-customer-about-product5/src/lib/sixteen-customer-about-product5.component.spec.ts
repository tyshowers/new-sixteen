import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerAboutProduct5Component } from './sixteen-customer-about-product5.component';

describe('SixteenCustomerAboutProduct5Component', () => {
  let component: SixteenCustomerAboutProduct5Component;
  let fixture: ComponentFixture<SixteenCustomerAboutProduct5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerAboutProduct5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerAboutProduct5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
