import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerAboutProduct5Component } from './sixteen-customer-about-product5.component';

@NgModule({
  declarations: [SixteenCustomerAboutProduct5Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerAboutProduct5Component]
})
export class SixteenCustomerAboutProduct5Module { }
