import { TestBed } from '@angular/core/testing';

import { SixteenCustomerAboutProduct5Service } from './sixteen-customer-about-product5.service';

describe('SixteenCustomerAboutProduct5Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerAboutProduct5Service = TestBed.get(SixteenCustomerAboutProduct5Service);
    expect(service).toBeTruthy();
  });
});
