import { Component, OnInit } from '@angular/core';
import { RemoteAssetService } from 'sixteen-services';


@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.css']
})
export class IntroComponent implements OnInit {

  public headingText = "Terms of Service";
  public backgroundImage = 'assets/terms-of-service.jpg';
  public bodyText;

  constructor(private _remoteAssetService: RemoteAssetService) { }

  ngOnInit() {
    this._remoteAssetService.getFileContents('./assets/terms.txt', this._remoteAssetService.TEXT)
    .subscribe({
      next: data => {this.bodyText = data; console.log(data)},
      error: err => console.error(err)
    })
  }

}
