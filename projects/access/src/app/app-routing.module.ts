import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignInSignOutModule } from './sign-in-sign-out/sign-in-sign-out.module';
import { IntroComponent } from './intro/intro.component';

const routes: Routes = [
  { path: 'terms', component: IntroComponent, data: { title: 'Application Access'} },
  { path: 'access', children: [ {path: '', loadChildren: './sign-in-sign-out/sign-in-sign-out.module#SignInSignOutModule'} ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
