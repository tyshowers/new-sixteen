import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {LoginComponent} from './login/login.component';
import {LogoutComponent} from './logout/logout.component';

const ROUTES: Routes = [
  {
    path: '',
    children: [
      { path: '', component: LoginComponent, data: { title: 'Login', state: 'login' } },
      { path: 'logout', component: LogoutComponent, data: { title: 'Login', state: 'login' } },
    ]
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(ROUTES),
  ],
  exports: [
    RouterModule
  ],
  providers: []
})
export class SignInSignOutRoutingModule { }
