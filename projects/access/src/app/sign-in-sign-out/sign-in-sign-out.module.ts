import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignInSignOutRoutingModule } from './sign-in-sign-out-routing.module';

import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { SixteenLoginModule } from 'sixteen-login';

import { environment } from '../../environments/environment';

export const firebaseConfig = environment.firebaseConfig;


@NgModule({
  declarations: [LoginComponent, LogoutComponent],
  imports: [
    CommonModule,
    SignInSignOutRoutingModule,
    SixteenLoginModule.forRoot(firebaseConfig)
  ]
})
export class SignInSignOutModule { }
