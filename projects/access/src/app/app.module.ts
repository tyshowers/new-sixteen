import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IntroComponent } from './intro/intro.component';
import { SixteenSimpleHeader1Module } from 'sixteen-simple-header1';
import { SixteenSimplePage1Module } from 'sixteen-simple-page1';
import { SixteenServicesModule } from 'sixteen-services';

@NgModule({
  declarations: [
    AppComponent,
    IntroComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SixteenSimpleHeader1Module,
    SixteenServicesModule,
    SixteenSimplePage1Module
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

@NgModule({})
export class AccessModule{
  static forRoot() : ModuleWithProviders{
    return {
      ngModule: AppModule,
      providers:[]
    }

  }
}
