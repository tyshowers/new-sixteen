import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerTryIt2Component } from './sixteen-customer-try-it2.component';

@NgModule({
  declarations: [SixteenCustomerTryIt2Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerTryIt2Component]
})
export class SixteenCustomerTryIt2Module { }
