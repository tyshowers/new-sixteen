import { TestBed } from '@angular/core/testing';

import { SixteenCustomerTryIt2Service } from './sixteen-customer-try-it2.service';

describe('SixteenCustomerTryIt2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerTryIt2Service = TestBed.get(SixteenCustomerTryIt2Service);
    expect(service).toBeTruthy();
  });
});
