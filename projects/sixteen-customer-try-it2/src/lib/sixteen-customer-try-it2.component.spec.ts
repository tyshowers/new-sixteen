import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerTryIt2Component } from './sixteen-customer-try-it2.component';

describe('SixteenCustomerTryIt2Component', () => {
  let component: SixteenCustomerTryIt2Component;
  let fixture: ComponentFixture<SixteenCustomerTryIt2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerTryIt2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerTryIt2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
