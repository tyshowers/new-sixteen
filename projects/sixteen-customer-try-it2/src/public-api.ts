/*
 * Public API Surface of sixteen-customer-try-it2
 */

export * from './lib/sixteen-customer-try-it2.service';
export * from './lib/sixteen-customer-try-it2.component';
export * from './lib/sixteen-customer-try-it2.module';
