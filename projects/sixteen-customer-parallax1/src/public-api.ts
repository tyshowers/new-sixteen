/*
 * Public API Surface of sixteen-customer-parallax1
 */

export * from './lib/sixteen-customer-parallax1.service';
export * from './lib/sixteen-customer-parallax1.component';
export * from './lib/sixteen-customer-parallax1.module';
