import { TestBed } from '@angular/core/testing';

import { SixteenCustomerParallax1Service } from './sixteen-customer-parallax1.service';

describe('SixteenCustomerParallax1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerParallax1Service = TestBed.get(SixteenCustomerParallax1Service);
    expect(service).toBeTruthy();
  });
});
