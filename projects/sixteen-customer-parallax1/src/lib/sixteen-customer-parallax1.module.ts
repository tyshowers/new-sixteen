import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerParallax1Component } from './sixteen-customer-parallax1.component';

@NgModule({
  declarations: [SixteenCustomerParallax1Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerParallax1Component]
})
export class SixteenCustomerParallax1Module { }
