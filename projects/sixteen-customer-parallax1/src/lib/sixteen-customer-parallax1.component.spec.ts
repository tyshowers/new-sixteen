import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerParallax1Component } from './sixteen-customer-parallax1.component';

describe('SixteenCustomerParallax1Component', () => {
  let component: SixteenCustomerParallax1Component;
  let fixture: ComponentFixture<SixteenCustomerParallax1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerParallax1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerParallax1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
