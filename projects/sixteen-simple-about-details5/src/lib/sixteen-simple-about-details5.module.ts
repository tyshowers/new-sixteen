import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenSimpleAboutDetails5Component } from './sixteen-simple-about-details5.component';

@NgModule({
  declarations: [SixteenSimpleAboutDetails5Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenSimpleAboutDetails5Component]
})
export class SixteenSimpleAboutDetails5Module { }
