import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenSimpleAboutDetails5Component } from './sixteen-simple-about-details5.component';

describe('SixteenSimpleAboutDetails5Component', () => {
  let component: SixteenSimpleAboutDetails5Component;
  let fixture: ComponentFixture<SixteenSimpleAboutDetails5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenSimpleAboutDetails5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenSimpleAboutDetails5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
