import { TestBed } from '@angular/core/testing';

import { SixteenSimpleAboutDetails5Service } from './sixteen-simple-about-details5.service';

describe('SixteenSimpleAboutDetails5Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenSimpleAboutDetails5Service = TestBed.get(SixteenSimpleAboutDetails5Service);
    expect(service).toBeTruthy();
  });
});
