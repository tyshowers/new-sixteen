import { TestBed } from '@angular/core/testing';

import { SixteenCustomerFaq2Service } from './sixteen-customer-faq2.service';

describe('SixteenCustomerFaq2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerFaq2Service = TestBed.get(SixteenCustomerFaq2Service);
    expect(service).toBeTruthy();
  });
});
