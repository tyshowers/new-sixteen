import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerFaq2Component } from './sixteen-customer-faq2.component';

@NgModule({
  declarations: [SixteenCustomerFaq2Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerFaq2Component]
})
export class SixteenCustomerFaq2Module { }
