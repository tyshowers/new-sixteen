import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerFaq2Component } from './sixteen-customer-faq2.component';

describe('SixteenCustomerFaq2Component', () => {
  let component: SixteenCustomerFaq2Component;
  let fixture: ComponentFixture<SixteenCustomerFaq2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerFaq2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerFaq2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
