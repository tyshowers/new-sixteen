import { TestBed } from '@angular/core/testing';

import { SixteenCustomerAboutProduct4Service } from './sixteen-customer-about-product4.service';

describe('SixteenCustomerAboutProduct4Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerAboutProduct4Service = TestBed.get(SixteenCustomerAboutProduct4Service);
    expect(service).toBeTruthy();
  });
});
