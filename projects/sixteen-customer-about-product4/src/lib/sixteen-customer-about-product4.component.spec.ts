import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerAboutProduct4Component } from './sixteen-customer-about-product4.component';

describe('SixteenCustomerAboutProduct4Component', () => {
  let component: SixteenCustomerAboutProduct4Component;
  let fixture: ComponentFixture<SixteenCustomerAboutProduct4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerAboutProduct4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerAboutProduct4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
