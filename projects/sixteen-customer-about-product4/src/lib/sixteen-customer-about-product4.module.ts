import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerAboutProduct4Component } from './sixteen-customer-about-product4.component';

@NgModule({
  declarations: [SixteenCustomerAboutProduct4Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerAboutProduct4Component]
})
export class SixteenCustomerAboutProduct4Module { }
