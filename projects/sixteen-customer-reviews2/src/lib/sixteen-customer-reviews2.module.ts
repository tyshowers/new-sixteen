import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SixteenCustomerReviews2Component } from './sixteen-customer-reviews2.component';
import { SixteenDataModule } from 'sixteen-data';

@NgModule({
  declarations: [SixteenCustomerReviews2Component],
  imports: [
    CommonModule,
    SixteenDataModule
  ],
  exports: [SixteenCustomerReviews2Component]
})
export class SixteenCustomerReviews2Module { }
