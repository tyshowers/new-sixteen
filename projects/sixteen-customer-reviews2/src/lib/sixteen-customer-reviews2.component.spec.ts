import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerReviews2Component } from './sixteen-customer-reviews2.component';

describe('SixteenCustomerReviews2Component', () => {
  let component: SixteenCustomerReviews2Component;
  let fixture: ComponentFixture<SixteenCustomerReviews2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerReviews2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerReviews2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
