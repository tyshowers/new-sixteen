import { TestBed } from '@angular/core/testing';

import { SixteenCustomerReviews2Service } from './sixteen-customer-reviews2.service';

describe('SixteenCustomerReviews2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerReviews2Service = TestBed.get(SixteenCustomerReviews2Service);
    expect(service).toBeTruthy();
  });
});
