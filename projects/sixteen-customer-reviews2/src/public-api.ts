/*
 * Public API Surface of sixteen-customer-reviews2
 */

export * from './lib/sixteen-customer-reviews2.service';
export * from './lib/sixteen-customer-reviews2.component';
export * from './lib/sixteen-customer-reviews2.module';
