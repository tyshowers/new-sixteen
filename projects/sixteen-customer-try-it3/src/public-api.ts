/*
 * Public API Surface of sixteen-customer-try-it3
 */

export * from './lib/sixteen-customer-try-it3.service';
export * from './lib/sixteen-customer-try-it3.component';
export * from './lib/sixteen-customer-try-it3.module';
