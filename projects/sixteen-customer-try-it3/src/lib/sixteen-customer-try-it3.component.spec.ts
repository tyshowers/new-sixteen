import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerTryIt3Component } from './sixteen-customer-try-it3.component';

describe('SixteenCustomerTryIt3Component', () => {
  let component: SixteenCustomerTryIt3Component;
  let fixture: ComponentFixture<SixteenCustomerTryIt3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerTryIt3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerTryIt3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
