import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerTryIt3Component } from './sixteen-customer-try-it3.component';

@NgModule({
  declarations: [SixteenCustomerTryIt3Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerTryIt3Component]
})
export class SixteenCustomerTryIt3Module { }
