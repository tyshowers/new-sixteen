import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lib-sixteen-customer-try-it3',
  templateUrl: './sixteen-customer-try-it3.component.html',
  styles: []
})
export class SixteenCustomerTryIt3Component implements OnInit {

  @Input() headingText = "Try it free for 14 day";
  @Input() descriptionText = "Quidem rerum necessitatibus saepe eveniet. Expedita distinctio quibusdam et harum quidem.";
  @Input() buttonLink;
  @Input() buttonText = "Start Free Trial";

  constructor() { }

  ngOnInit() {
  }

}
