import { TestBed } from '@angular/core/testing';

import { SixteenCustomerTryIt3Service } from './sixteen-customer-try-it3.service';

describe('SixteenCustomerTryIt3Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerTryIt3Service = TestBed.get(SixteenCustomerTryIt3Service);
    expect(service).toBeTruthy();
  });
});
