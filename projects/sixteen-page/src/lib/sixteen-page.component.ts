import { Component, OnInit, Input, ViewChild, ElementRef, OnDestroy, AfterViewInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { Subject } from 'rxjs';
import {debounceTime} from 'rxjs/operators';


@Component({
  selector: 'lib-sixteen-page',
  templateUrl: './sixteen-page.component.html',
  styles: []
})
export class SixteenPageComponent implements OnInit, OnDestroy, AfterViewInit {
  // message trying to login
  public diagDisplay = "none";
  // Toggles the page's diagnostic information
  public toggleDisplay = false;

  @Input() diagnostics;

  @ViewChild("help") private helpModal: ElementRef;

  // Status of the alert that will display the message, false = hidden
  public staticAlertClosed = false;
  // Result of closing the help
  public closeResult: string;
  // String to display on alert
  public successMessage: string;
  // message to display once user hits submit to login
  public success = new Subject<string>();
  // Type of alert
  public alertType = "success";

  public squareView = false;

  public listView = false;

  public sliderView = false;

  public messageFeed: boolean = false;

  constructor(public modalService: NgbModal) {
  }

  ngOnInit() {
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this.success.subscribe((message) => this.successMessage = message);
    this.success.pipe(debounceTime(5000)).subscribe(() => this.successMessage = null);
  }

  ngOnDestroy() {
    if (this.success)
      this.success.unsubscribe();
  }

  ngAfterViewInit() {
    setTimeout(() => this.openHelp(), 1500)
  }

  public toggleSquareView() : void {
    this.listView = false;
    this.squareView = true;
    this.sliderView = false;
  }

  public toggleListView() : void {
    this.listView = true;
    this.squareView = false;
    this.sliderView = false;
  }

  public toggleSliderView() : void {
    this.sliderView = true;
    this.squareView = false;
    this.listView = false;
  }

  public openHelp() {
    if (this.helpModal)
      this.open(this.helpModal);

  }

  public open(help): void {
    this.modalService.open(help, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }


  public changeSuccessMessage(message: String, type: string): void {
    this.alertType = type;
    this.success.next(message + `  - ${new Date()}.`);
  }

  public toggleMessageFeed() : void {
    this.messageFeed = !this.messageFeed;
  }

  public toggleDiagnostic(): void {
    this.diagDisplay = (this.diagDisplay == "none") ? "" : "none";
    this.toggleDisplay = (this.toggleDisplay) ? false : true;
  }

  get diagnostic() {
    return this.diagnostics;
  }


}
