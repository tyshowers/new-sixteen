import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenPageComponent } from './sixteen-page.component';

describe('SixteenPageComponent', () => {
  let component: SixteenPageComponent;
  let fixture: ComponentFixture<SixteenPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
