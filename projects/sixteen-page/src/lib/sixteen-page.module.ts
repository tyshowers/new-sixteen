import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SixteenPageComponent } from './sixteen-page.component';

@NgModule({
  declarations: [SixteenPageComponent],
  imports: [ CommonModule ],
  exports: [SixteenPageComponent]
})
export class SixteenPageModule { }
