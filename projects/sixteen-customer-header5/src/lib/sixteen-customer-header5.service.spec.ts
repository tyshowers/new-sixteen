import { TestBed } from '@angular/core/testing';

import { SixteenCustomerHeader5Service } from './sixteen-customer-header5.service';

describe('SixteenCustomerHeader5Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerHeader5Service = TestBed.get(SixteenCustomerHeader5Service);
    expect(service).toBeTruthy();
  });
});
