import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerHeader5Component } from './sixteen-customer-header5.component';

describe('SixteenCustomerHeader5Component', () => {
  let component: SixteenCustomerHeader5Component;
  let fixture: ComponentFixture<SixteenCustomerHeader5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerHeader5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerHeader5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
