import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerHeader5Component } from './sixteen-customer-header5.component';

@NgModule({
  declarations: [SixteenCustomerHeader5Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerHeader5Component]
})
export class SixteenCustomerHeader5Module { }
