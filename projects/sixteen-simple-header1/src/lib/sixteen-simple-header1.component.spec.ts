import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenSimpleHeader1Component } from './sixteen-simple-header1.component';

describe('SixteenSimpleHeader1Component', () => {
  let component: SixteenSimpleHeader1Component;
  let fixture: ComponentFixture<SixteenSimpleHeader1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenSimpleHeader1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenSimpleHeader1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
