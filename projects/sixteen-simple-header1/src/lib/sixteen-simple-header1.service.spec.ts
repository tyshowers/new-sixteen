import { TestBed } from '@angular/core/testing';

import { SixteenSimpleHeader1Service } from './sixteen-simple-header1.service';

describe('SixteenSimpleHeader1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenSimpleHeader1Service = TestBed.get(SixteenSimpleHeader1Service);
    expect(service).toBeTruthy();
  });
});
