import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lib-sixteen-simple-header1',
  templateUrl: './sixteen-simple-header1.component.html',
  styles: []
})
export class SixteenSimpleHeader1Component implements OnInit {

  @Input() headingText = 'Simple page';
  @Input() backgroundImage = 'http://via.placeholder.com/1920x1275';

  constructor() { }

  ngOnInit() {
  }

}
