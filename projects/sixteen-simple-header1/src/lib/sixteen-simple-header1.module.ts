import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenSimpleHeader1Component } from './sixteen-simple-header1.component';

@NgModule({
  declarations: [SixteenSimpleHeader1Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenSimpleHeader1Component]
})
export class SixteenSimpleHeader1Module { }
