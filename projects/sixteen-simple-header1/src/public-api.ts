/*
 * Public API Surface of sixteen-simple-header1
 */

export * from './lib/sixteen-simple-header1.service';
export * from './lib/sixteen-simple-header1.component';
export * from './lib/sixteen-simple-header1.module';
