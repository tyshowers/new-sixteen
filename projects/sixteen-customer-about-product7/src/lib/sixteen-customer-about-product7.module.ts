import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenAboutProduct7Component } from './sixteen-customer-about-product7.component';

@NgModule({
  declarations: [SixteenAboutProduct7Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenAboutProduct7Component]
})
export class SixteenCustomerAboutProduct7Module { }
