import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenAboutProduct7Component } from './sixteen-customer-about-product7.component';

describe('SixteenAboutProduct7Component', () => {
  let component: SixteenAboutProduct7Component;
  let fixture: ComponentFixture<SixteenAboutProduct7Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenAboutProduct7Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenAboutProduct7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
