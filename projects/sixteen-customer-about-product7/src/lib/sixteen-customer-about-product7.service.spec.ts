import { TestBed } from '@angular/core/testing';

import { SixteenAboutProduct7Service } from './sixteen-customer-about-product7.service';

describe('SixteenAboutProduct7Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenAboutProduct7Service = TestBed.get(SixteenAboutProduct7Service);
    expect(service).toBeTruthy();
  });
});
