import { TestBed } from '@angular/core/testing';

import { SixteenCustomerAboutProductFeatures2Service } from './sixteen-customer-product-features2.service';

describe('SixteenCustomerAboutProductFeatures2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerAboutProductFeatures2Service = TestBed.get(SixteenCustomerAboutProductFeatures2Service);
    expect(service).toBeTruthy();
  });
});
