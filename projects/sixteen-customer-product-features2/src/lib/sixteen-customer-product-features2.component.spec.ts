import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerProductFeatures2Component } from './sixteen-customer-product-features2.component';

describe('SixteenCustomerProductFeatures2Component', () => {
  let component: SixteenCustomerProductFeatures2Component;
  let fixture: ComponentFixture<SixteenCustomerProductFeatures2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerProductFeatures2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerProductFeatures2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
