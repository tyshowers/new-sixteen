import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerProductFeatures2Component } from './sixteen-customer-product-features2.component';

@NgModule({
  declarations: [SixteenCustomerProductFeatures2Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerProductFeatures2Component]
})
export class SixteenCustomerProductFeatures2Module { }
