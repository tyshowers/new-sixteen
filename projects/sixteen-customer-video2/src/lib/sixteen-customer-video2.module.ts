import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerVideo2Component } from './sixteen-customer-video2.component';

@NgModule({
  declarations: [SixteenCustomerVideo2Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerVideo2Component]
})
export class SixteenCustomerVideo2Module { }
