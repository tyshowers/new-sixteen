import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerVideo2Component } from './sixteen-customer-video2.component';

describe('SixteenCustomerVideo2Component', () => {
  let component: SixteenCustomerVideo2Component;
  let fixture: ComponentFixture<SixteenCustomerVideo2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerVideo2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerVideo2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
