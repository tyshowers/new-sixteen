import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lib-sixteen-customer-video2',
  templateUrl: './sixteen-customer-video2.component.html',
  styles: []
})
export class SixteenCustomerVideo2Component implements OnInit {

  @Input() headingText = "Video";
  @Input() descriptionText = "Quo voluptas assumenda est, qui blanditiis praesentium voluptatum explicabo rem aperiam deleniti atque. Esse, quam nihil molestiae non numquam eius modi tempora incidunt."
  @Input() videoURL = "https://www.youtube.com/watch?v=R7ZDLGfi2Wo";
  @Input() imageURL = "http://via.placeholder.com/1450x816";

  constructor() { }

  ngOnInit() {
  }

}
