import { TestBed } from '@angular/core/testing';

import { SixteenCustomerVideo2Service } from './sixteen-customer-video2.service';

describe('SixteenCustomerVideo2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerVideo2Service = TestBed.get(SixteenCustomerVideo2Service);
    expect(service).toBeTruthy();
  });
});
