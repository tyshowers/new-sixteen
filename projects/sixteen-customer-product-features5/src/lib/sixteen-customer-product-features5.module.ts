import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerProductFeatures5Component } from './sixteen-customer-product-features5.component';

@NgModule({
  declarations: [SixteenCustomerProductFeatures5Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerProductFeatures5Component]
})
export class SixteenCustomerProductFeatures5Module { }
