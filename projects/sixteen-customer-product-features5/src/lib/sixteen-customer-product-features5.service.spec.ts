import { TestBed } from '@angular/core/testing';

import { SixteenCustomerProductFeatures5Service } from './sixteen-customer-product-features5.service';

describe('SixteenCustomerProductFeatures5Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerProductFeatures5Service = TestBed.get(SixteenCustomerProductFeatures5Service);
    expect(service).toBeTruthy();
  });
});
