import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerProductFeatures5Component } from './sixteen-customer-product-features5.component';

describe('SixteenCustomerProductFeatures5Component', () => {
  let component: SixteenCustomerProductFeatures5Component;
  let fixture: ComponentFixture<SixteenCustomerProductFeatures5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerProductFeatures5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerProductFeatures5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
