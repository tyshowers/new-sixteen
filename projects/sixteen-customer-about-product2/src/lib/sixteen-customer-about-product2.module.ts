import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerAboutProduct2Component } from './sixteen-customer-about-product2.component';

@NgModule({
  declarations: [SixteenCustomerAboutProduct2Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerAboutProduct2Component]
})
export class SixteenCustomerAboutProduct2Module { }
