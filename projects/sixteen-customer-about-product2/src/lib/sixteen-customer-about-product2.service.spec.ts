import { TestBed } from '@angular/core/testing';

import { SixteenCustomerAboutProduct2Service } from './sixteen-customer-about-product2.service';

describe('SixteenCustomerAboutProduct2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerAboutProduct2Service = TestBed.get(SixteenCustomerAboutProduct2Service);
    expect(service).toBeTruthy();
  });
});
