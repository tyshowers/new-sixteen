import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerAboutProduct2Component } from './sixteen-customer-about-product2.component';

describe('SixteenCustomerAboutProduct2Component', () => {
  let component: SixteenCustomerAboutProduct2Component;
  let fixture: ComponentFixture<SixteenCustomerAboutProduct2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerAboutProduct2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerAboutProduct2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
