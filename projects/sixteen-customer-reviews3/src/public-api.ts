/*
 * Public API Surface of sixteen-customer-reviews3
 */

export * from './lib/sixteen-customer-reviews3.service';
export * from './lib/sixteen-customer-reviews3.component';
export * from './lib/sixteen-customer-reviews3.module';
