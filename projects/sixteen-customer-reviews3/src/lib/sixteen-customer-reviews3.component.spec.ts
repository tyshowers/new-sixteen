import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerReviews3Component } from './sixteen-customer-reviews3.component';

describe('SixteenCustomerReviews3Component', () => {
  let component: SixteenCustomerReviews3Component;
  let fixture: ComponentFixture<SixteenCustomerReviews3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerReviews3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerReviews3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
