import { NgModule } from '@angular/core';
import { SixteenCustomerReviews3Component } from './sixteen-customer-reviews3.component';
import { CommonModule } from '@angular/common';
import { SixteenDataModule } from 'sixteen-data';

@NgModule({
  declarations: [SixteenCustomerReviews3Component],
  imports: [
    CommonModule,
    SixteenDataModule
  ],
  exports: [SixteenCustomerReviews3Component]
})
export class SixteenCustomerReviews3Module { }
