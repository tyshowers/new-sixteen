import { Component, OnInit, Input } from '@angular/core';
import { Review } from 'sixteen-data';

@Component({
  selector: 'lib-sixteen-customer-reviews3',
  templateUrl: './sixteen-customer-reviews3.component.html',
  styles: []
})
export class SixteenCustomerReviews3Component implements OnInit {

  @Input() headingText = 'Reviews';
  @Input() descriptionText = 'Earum rerum necessitatibus saepe eveniet, ut labore. Dicta sunt, explicabo laborum et harum quidem rerum hic tenetur. Itaque earum rerum hic tenetur a sapiente delectus, ut et.';
  @Input() reviews : Review[];

  constructor() { }

  ngOnInit() {
  }

}
