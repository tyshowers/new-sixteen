import { TestBed } from '@angular/core/testing';

import { SixteenCustomerReviews3Service } from './sixteen-customer-reviews3.service';

describe('SixteenCustomerReviews3Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerReviews3Service = TestBed.get(SixteenCustomerReviews3Service);
    expect(service).toBeTruthy();
  });
});
