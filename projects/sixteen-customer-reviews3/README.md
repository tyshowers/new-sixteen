# SixteenCustomerReviews3

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.0.

## Code scaffolding

Run `ng generate component component-name --project sixteen-customer-reviews3` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project sixteen-customer-reviews3`.
> Note: Don't forget to add `--project sixteen-customer-reviews3` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build sixteen-customer-reviews3` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build sixteen-customer-reviews3`, go to the dist folder `cd dist/sixteen-customer-reviews3` and run `npm publish`.

## Running unit tests

Run `ng test sixteen-customer-reviews3` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
