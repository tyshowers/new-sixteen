/*
 * Public API Surface of sixteen-customer-team3
 */

export * from './lib/sixteen-customer-team3.service';
export * from './lib/sixteen-customer-team3.component';
export * from './lib/sixteen-customer-team3.module';
