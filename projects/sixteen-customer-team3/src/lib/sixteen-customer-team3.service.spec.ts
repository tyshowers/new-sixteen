import { TestBed } from '@angular/core/testing';

import { SixteenCustomerTeam3Service } from './sixteen-customer-team3.service';

describe('SixteenCustomerTeam3Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerTeam3Service = TestBed.get(SixteenCustomerTeam3Service);
    expect(service).toBeTruthy();
  });
});
