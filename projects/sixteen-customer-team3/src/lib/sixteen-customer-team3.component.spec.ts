import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerTeam3Component } from './sixteen-customer-team3.component';

describe('SixteenCustomerTeam3Component', () => {
  let component: SixteenCustomerTeam3Component;
  let fixture: ComponentFixture<SixteenCustomerTeam3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerTeam3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerTeam3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
