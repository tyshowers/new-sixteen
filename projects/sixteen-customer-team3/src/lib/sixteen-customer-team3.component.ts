import { Component, OnInit, Input } from '@angular/core';
import { User } from 'sixteen-data';

@Component({
  selector: 'lib-sixteen-customer-team3',
  templateUrl: './sixteen-customer-team3.component.html',
  styles: []
})
export class SixteenCustomerTeam3Component implements OnInit {

  @Input() headingText = "Our Team";
  @Input() descriptionText = "Saepe eveniet, ut perspiciatis, unde omnis iste natus sit voluptatem sequi. Deleniti atque corrupti, quos dolores. Accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore.";
  @Input() users : User[];

  constructor() { }

  ngOnInit() {
  }

}
