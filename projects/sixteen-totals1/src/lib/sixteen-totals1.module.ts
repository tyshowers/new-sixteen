import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenTotals1Component } from './sixteen-totals1.component';
import { CountToModule } from 'angular-count-to';

@NgModule({
  declarations: [SixteenTotals1Component],
  imports: [
    CommonModule,
    RouterModule,
    CountToModule
  ],
  exports: [SixteenTotals1Component]
})
export class SixteenTotals1Module { }
