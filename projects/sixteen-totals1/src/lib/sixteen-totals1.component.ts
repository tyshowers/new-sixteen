import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lib-sixteen-totals1',
  templateUrl: './sixteen-totals1.component.html',
  styles: []
})
export class SixteenTotals1Component implements OnInit {


  @Input() count1: number = 800000;
  @Input() countText1 = "Downloads";

  @Input() count2: number = 700000;
  @Input() countText2 = "Daily Views";

  @Input() count3: number = 600000;
  @Input() countText3 = "Active Users";

  @Input() count4: number = 500000;
  @Input() countText4 = "plugins";

  constructor() { }

  ngOnInit() {
  }

}
