import { TestBed } from '@angular/core/testing';

import { SixteenTotals1Service } from './sixteen-totals1.service';

describe('SixteenTotals1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenTotals1Service = TestBed.get(SixteenTotals1Service);
    expect(service).toBeTruthy();
  });
});
