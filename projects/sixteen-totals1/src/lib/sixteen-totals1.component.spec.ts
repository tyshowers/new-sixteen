import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenTotals1Component } from './sixteen-totals1.component';

describe('SixteenTotals1Component', () => {
  let component: SixteenTotals1Component;
  let fixture: ComponentFixture<SixteenTotals1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenTotals1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenTotals1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
