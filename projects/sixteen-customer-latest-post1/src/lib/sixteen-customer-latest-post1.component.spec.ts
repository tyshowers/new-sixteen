import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerLatestPost1Component } from './sixteen-customer-latest-post1.component';

describe('SixteenCustomerLatestPost1Component', () => {
  let component: SixteenCustomerLatestPost1Component;
  let fixture: ComponentFixture<SixteenCustomerLatestPost1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerLatestPost1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerLatestPost1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
