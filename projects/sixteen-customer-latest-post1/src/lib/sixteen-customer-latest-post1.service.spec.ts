import { TestBed } from '@angular/core/testing';

import { SixteenCustomerLatestPost1Service } from './sixteen-customer-latest-post1.service';

describe('SixteenCustomerLatestPost1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerLatestPost1Service = TestBed.get(SixteenCustomerLatestPost1Service);
    expect(service).toBeTruthy();
  });
});
