import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerLatestPost1Component } from './sixteen-customer-latest-post1.component';
import { SixteenDataModule } from 'sixteen-data';

@NgModule({
  declarations: [SixteenCustomerLatestPost1Component],
  imports: [
    CommonModule,
    RouterModule,
    SixteenDataModule
  ],
  exports: [SixteenCustomerLatestPost1Component]
})
export class SixteenCustomerLatestPost1Module { }
