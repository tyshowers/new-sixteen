import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenSimpleAboutDetails6Component } from './sixteen-simple-about-details6.component';

@NgModule({
  declarations: [SixteenSimpleAboutDetails6Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenSimpleAboutDetails6Component]
})
export class SixteenSimpleAboutDetails6Module { }
