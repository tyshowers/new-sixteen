import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenSimpleAboutDetails6Component } from './sixteen-simple-about-details6.component';

describe('SixteenSimpleAboutDetails6Component', () => {
  let component: SixteenSimpleAboutDetails6Component;
  let fixture: ComponentFixture<SixteenSimpleAboutDetails6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenSimpleAboutDetails6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenSimpleAboutDetails6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
