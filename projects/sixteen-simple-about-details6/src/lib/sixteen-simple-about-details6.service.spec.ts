import { TestBed } from '@angular/core/testing';

import { SixteenSimpleAboutDetails6Service } from './sixteen-simple-about-details6.service';

describe('SixteenSimpleAboutDetails6Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenSimpleAboutDetails6Service = TestBed.get(SixteenSimpleAboutDetails6Service);
    expect(service).toBeTruthy();
  });
});
