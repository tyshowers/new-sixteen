import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenMenuComponent } from './sixteen-menu.component';

describe('SixteenMenuComponent', () => {
  let component: SixteenMenuComponent;
  let fixture: ComponentFixture<SixteenMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
