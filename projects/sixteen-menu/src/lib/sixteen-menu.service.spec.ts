import { TestBed } from '@angular/core/testing';

import { SixteenMenuService } from './sixteen-menu.service';

describe('SixteenMenuService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenMenuService = TestBed.get(SixteenMenuService);
    expect(service).toBeTruthy();
  });
});
