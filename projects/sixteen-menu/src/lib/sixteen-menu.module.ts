import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenMenuComponent } from './sixteen-menu.component';
import { SixteenDataModule } from 'sixteen-data';

@NgModule({
  declarations: [SixteenMenuComponent],
  imports: [
    CommonModule,
    RouterModule,
    SixteenDataModule
  ],
  exports: [SixteenMenuComponent]
})
export class SixteenMenuModule { }
