import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerVideo1Component } from './sixteen-customer-video1.component';

describe('SixteenCustomerVideo1Component', () => {
  let component: SixteenCustomerVideo1Component;
  let fixture: ComponentFixture<SixteenCustomerVideo1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerVideo1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerVideo1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
