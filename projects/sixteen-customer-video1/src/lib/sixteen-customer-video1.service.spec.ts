import { TestBed } from '@angular/core/testing';

import { SixteenCustomerVideo1Service } from './sixteen-customer-video1.service';

describe('SixteenCustomerVideo1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerVideo1Service = TestBed.get(SixteenCustomerVideo1Service);
    expect(service).toBeTruthy();
  });
});
