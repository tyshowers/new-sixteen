import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerVideo1Component } from './sixteen-customer-video1.component';

@NgModule({
  declarations: [SixteenCustomerVideo1Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerVideo1Component]
})
export class SixteenCustomerVideo1Module { }
