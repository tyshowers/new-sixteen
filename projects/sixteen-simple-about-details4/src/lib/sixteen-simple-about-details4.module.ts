import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenSimpleAboutDetails4Component } from './sixteen-simple-about-details4.component';

@NgModule({
  declarations: [SixteenSimpleAboutDetails4Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenSimpleAboutDetails4Component]
})
export class SixteenSimpleAboutDetails4Module { }
