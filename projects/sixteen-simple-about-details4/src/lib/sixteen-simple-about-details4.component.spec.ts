import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenSimpleAboutDetails4Component } from './sixteen-simple-about-details4.component';

describe('SixteenSimpleAboutDetails4Component', () => {
  let component: SixteenSimpleAboutDetails4Component;
  let fixture: ComponentFixture<SixteenSimpleAboutDetails4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenSimpleAboutDetails4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenSimpleAboutDetails4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
