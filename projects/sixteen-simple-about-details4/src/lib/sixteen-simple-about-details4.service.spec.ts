import { TestBed } from '@angular/core/testing';

import { SixteenSimpleAboutDetails4Service } from './sixteen-simple-about-details4.service';

describe('SixteenSimpleAboutDetails4Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenSimpleAboutDetails4Service = TestBed.get(SixteenSimpleAboutDetails4Service);
    expect(service).toBeTruthy();
  });
});
