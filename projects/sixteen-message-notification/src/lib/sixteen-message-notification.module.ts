import { NgModule } from '@angular/core';
import { SixteenMessageNotificationComponent } from './sixteen-message-notification.component';

@NgModule({
  declarations: [SixteenMessageNotificationComponent],
  imports: [
  ],
  exports: [SixteenMessageNotificationComponent]
})
export class SixteenMessageNotificationModule { }
