import { TestBed } from '@angular/core/testing';

import { SixteenMessageNotificationService } from './sixteen-message-notification.service';

describe('SixteenMessageNotificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenMessageNotificationService = TestBed.get(SixteenMessageNotificationService);
    expect(service).toBeTruthy();
  });
});
