import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenMessageNotificationComponent } from './sixteen-message-notification.component';

describe('SixteenMessageNotificationComponent', () => {
  let component: SixteenMessageNotificationComponent;
  let fixture: ComponentFixture<SixteenMessageNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenMessageNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenMessageNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
