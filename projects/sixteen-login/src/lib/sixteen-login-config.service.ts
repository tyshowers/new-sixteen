import { InjectionToken } from '@angular/core';
import { FirebaseConfig } from './firebase-config.model';

export const SixteenLoginConfigService = new InjectionToken<FirebaseConfig>("FirebaseConfig");
