import { Component, OnInit, OnDestroy, EventEmitter, Output, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SixteenPageComponent } from 'sixteen-page';
import { SixteenLoginService } from './sixteen-login.service';

@Component({
  selector: 'lib-sixteen-login',
  templateUrl: './sixteen-login.component.html',
  styleUrls: ['./sixteen-login.component.css']
})
export class SixteenLoginComponent extends SixteenPageComponent implements OnInit, OnDestroy {


  emailAddress = '';

  password = '';

  appUser = null;

  @Output() theUser = new EventEmitter<any>();

  verified = false;

  @Input() readonly CORPORATE_ACCOUNT = '';

  @Input() readonly COMPANY_NAME = '';

  private _userSubscription: Subscription;


  constructor(public modalService: NgbModal, public router: Router, private _sixteenLoginService: SixteenLoginService) {
    super(modalService);
  }

  ngOnInit() {
    this._userSubscription = this._sixteenLoginService.firebaseUser.subscribe(firebaseUser => {
      this.appUser = firebaseUser;
      this.onUserUpdate();
    });
  }

  onUserUpdate() {
    console.log("appUser = " + JSON.stringify( this.appUser));
    this.theUser.emit(this.appUser);
  }

  ngOnDestroy() {
    if (this._userSubscription)
      this._userSubscription.unsubscribe();
  }

  loginWithGoogle() {
    this._sixteenLoginService.signInWithGoogle();
  }

  loginWithTwitter() {
    // TODO
  }

  loginWithFacebook() {
    // TODO
  }

  resendConfirmation() {
    // TODO
  }

  onSubmit() {
    this._sixteenLoginService.signInWithUserNameAndPassword(this.emailAddress, this.password);
  }

  onLogOut() {
    this._sixteenLoginService.signOut();
    setTimeout(() => { window.location.reload(); }, 1000);

  }

  get diagnostic() {
    return JSON.stringify(this.appUser, null, '\t')
  }


}
