import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SixteenLoginComponent } from './sixteen-login.component';
import { SixteenLoginService} from './sixteen-login.service';
import { SixteenLoginConfigService} from './sixteen-login-config.service';
import { SixteenDataModule } from 'sixteen-data';
import { SixteenPageModule } from 'sixteen-page';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FirebaseConfig } from './firebase-config.model';



@NgModule({
  declarations: [SixteenLoginComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgbModule.forRoot(),
    SixteenDataModule,
    SixteenPageModule
  ],
  exports: [SixteenLoginComponent]
})

export class SixteenLoginModule {

  static forRoot(config: FirebaseConfig): ModuleWithProviders {
    // console.log("Environment passed=" + JSON.stringify(config));
    return {
      ngModule: SixteenLoginModule,
      providers: [
        SixteenLoginService,
        {
          provide: SixteenLoginConfigService,
          useValue: config
        }
      ]
    }
  }

  static forChild() : ModuleWithProviders {
    return {
      ngModule: SixteenLoginModule
    }
  }
}
