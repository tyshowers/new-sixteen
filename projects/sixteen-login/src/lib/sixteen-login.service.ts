import { Injectable, Inject } from '@angular/core';
import { SixteenLoginConfigService } from './sixteen-login-config.service';

import * as firebase from 'firebase';

import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SixteenLoginService {

  public error = new Subject<any>();
  public errorMessage = new Subject<any>();
  public processMessage = new Subject<any>();

  public firebaseUser = new Subject<firebase.User>();



  constructor(@Inject(SixteenLoginConfigService) private config) {
    this.initFirebase()
  }

  private initFirebase() {
    firebase.initializeApp(this.config);

    this.userStateChange();
  }

  private userStateChange() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.firebaseUser.next(user);
      }
    })
  }

  public signInWithUserNameAndPassword(emailAddress, password) {
    firebase.auth().createUserWithEmailAndPassword(emailAddress, password).catch((error) => {
      this.error.next(error.code);
      this.errorMessage.next(error.message);
    })
  }

  public signInWithGoogle() {
    var provider = new firebase.auth.GoogleAuthProvider();

    provider.addScope('https://www.googleapis.com/auth/plus.login');

    firebase.auth().signInWithRedirect(provider);

    firebase.auth().getRedirectResult().then((authData) => {
      console.log(authData);
    }).catch(function(error) {
      console.log(error);
    });
  }

  public signOut() {
    firebase.auth().signOut().then(() => {
      // Sign-out successful.
    }, function(error) {
      console.log(error);
    });
  }


  public sendEmailVerification() {
    firebase.auth().currentUser.sendEmailVerification().then(() => {
      this.processMessage.next('Email Verification Sent!')
    });
  }

  public sendPasswordReset(emailAddress) {
    firebase.auth().sendPasswordResetEmail(emailAddress).then(() => {
      this.processMessage.next('Password Reset Email Sent!');
    }).catch((error) => {
      var errorCode = error.code;

      if (errorCode == 'auth/invalid-email') {
        this.errorMessage.next(error.message);
      } else if (errorCode == 'auth/user-not-found') {
        this.errorMessage.next(error.message);
      }
      console.log(error);
    });
  }
}
