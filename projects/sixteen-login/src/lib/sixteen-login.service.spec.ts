import { TestBed } from '@angular/core/testing';

import { SixteenLoginService } from './sixteen-login.service';

describe('SixteenLoginService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenLoginService = TestBed.get(SixteenLoginService);
    expect(service).toBeTruthy();
  });
});
