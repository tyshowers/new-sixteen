import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenLoginComponent } from './sixteen-login.component';

describe('SixteenLoginComponent', () => {
  let component: SixteenLoginComponent;
  let fixture: ComponentFixture<SixteenLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
