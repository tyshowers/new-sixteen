/*
 * Public API Surface of sixteen-customer-integration2
 */

export * from './lib/sixteen-customer-integration2.service';
export * from './lib/sixteen-customer-integration2.component';
export * from './lib/sixteen-customer-integration2.module';
