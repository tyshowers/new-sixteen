import { TestBed } from '@angular/core/testing';

import { SixteenCustomerIntegration2Service } from './sixteen-customer-integration2.service';

describe('SixteenCustomerIntegration2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SixteenCustomerIntegration2Service = TestBed.get(SixteenCustomerIntegration2Service);
    expect(service).toBeTruthy();
  });
});
