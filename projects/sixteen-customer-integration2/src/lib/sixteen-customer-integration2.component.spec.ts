import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixteenCustomerIntegration2Component } from './sixteen-customer-integration2.component';

describe('SixteenCustomerIntegration2Component', () => {
  let component: SixteenCustomerIntegration2Component;
  let fixture: ComponentFixture<SixteenCustomerIntegration2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixteenCustomerIntegration2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixteenCustomerIntegration2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
