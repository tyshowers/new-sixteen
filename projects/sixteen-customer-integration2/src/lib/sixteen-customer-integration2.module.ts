import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SixteenCustomerIntegration2Component } from './sixteen-customer-integration2.component';

@NgModule({
  declarations: [SixteenCustomerIntegration2Component],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SixteenCustomerIntegration2Component]
})
export class SixteenCustomerIntegration2Module { }
